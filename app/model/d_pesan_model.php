<?php
class D_Pesan_Model extends SENE_Model{
  var $tbl = 'd_pesan';
  var $tbl_as = 'g';

  public function __construct(){
    parent::__construct();
  }
  public function getAll(){
    $this->db->select('*')->from($this->tbl);
    return $this->db->get("object",0);
  }
  public function insert($dataArray=array()){
    return $this->db->insert($this->tbl,$dataArray);
  }
  public function update($id,$dataUpdate=array()){
    $this->db->where("id",$id);
    return $this->db->update($this->tbl,$dataUpdate);
  }
  public function delete($id){
    $this->db->where("id",$id);
    return $this->db->delete($this->tbl);
  }
  public function getById($id){
    $this->db->where("id",$id);
    $this->db->from($this->tbl,$this->tbl_as);
    return $this->db->get_first();
  }
}
