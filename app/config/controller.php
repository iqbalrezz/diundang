<?php
$default_controller='home';
$notfound_controller='notfound';

//override default routing
//make sure dont add any traing slash in array key of routes
$routes['kategori/(:any)'] = 'kategori/detail/$1';
$routes['kategori/'] = 'kategori/index/$1';
$routes['blog/(:any)'] = 'blog/detail/$1';
$routes['blog/kategori/detail/(:any)'] = 'blog/kategori/$1';
$routes['blog/kategori'] = 'blog/kategori';
$routes['blog/'] = 'blog/index/$1';
$routes['produk/(:any)'] = 'produk/detail/$1';
$routes['produk/'] = 'produk/index/$1';
//example $routes['blog/id/(:num)/(:any)'] = 'blog/detail/index/$1/$2';
$routes['kajian/(:any)/(:any)/(:any)'] = 'kajian/detail/$1/$2/$3';
$routes['kajian/(:any)/(:any)'] = 'kajian/index/$1/$2';
$routes['kajian/(:any)'] = 'kajian/index/$1';
$routes['kajian/'] = 'kajian/index/$1';

$routes['kegiatan/(:any)/(:any)/(:any)'] = 'kegiatan/detail/$1/$2/$3';
$routes['kegiatan/(:any)/(:any)'] = 'kegiatan/index/$1/$2';
$routes['kegiatan/(:any)'] = 'kegiatan/index/$1';
$routes['kegiatan/'] = 'kegiatan/index/$1';
