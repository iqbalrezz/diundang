(function() {

//	default opts:
//	parallaxMultiple: 0.1,
//	tiltMultiple: 0.05,
//      duration: 500,
//      easing:'easeOutExpo'

    $('.tilt_bg').parallaxTilt({
      parallaxMultiple: .15
    });
    $('.tilt_human').parallaxTilt({
      parallaxMultiple: .16
    });
    $('.tilt_share1').parallaxTilt({
      parallaxMultiple: .10
    });
    $('.tilt_share2').parallaxTilt({
      parallaxMultiple: .10
    });

})();
