<style>
.butn-bord{
  margin-top:5px;
}
.btn-offer{
  background:#75dab0;
  border-color:#75dab0;
}
.btn-offer:hover{
  background: #06bb70;
  background: #06bb70;
}

.fitur{
  margin-bottom: 120px;

}
.kartu-fitur{
  box-shadow:2px 2px 2px 2px;
}
.icon-fitur i{
  border:2px solid #75dab0;
  border-radius: 50%;
  padding:24px;
  color:#75dab0;
  font-size:24px;
}
.icon-fitur:hover i{
  color:#ffffff;
  background:#000000;
  border: #000000;
}
.ngedit{
  margin-left:250px
}
.parallaxTilt_hover{
  margin-top: 200px;
}



</style>

<section id="home-fitur" class="fitur">
  <div class="container">
    <div class="row">
      <h2 style="text-align:center;font-size:3em;">Fitur</h2>
      <br/>
      <div class="col-md-5">
        <div class="row">
          <div class="col-md-12">
            <div class="kartu-fitur">
              <div class="text-center icon-fitur">
                <i class="fa fa-picture-o" aria-hidden="true"></i>
              </div>
              <h3>Eye Catching Design</h3>
              <p>Desain dibuat semenarik mungkin sehingga yang <b>Diundang</b> tidak bosan melihatnya</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="kartu-fitur">
              <div class="text-center icon-fitur">
                <i class="fa fa-hand-o-up" aria-hidden="true"></i>
              </div>
              <h3>Simple dan Informatif</h3>
              <p>Dibuat simple agar yang <b>Diundang</b> langsung mendapatkan info yang diinginkan</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="kartu-fitur">
              <div class="text-center icon-fitur">
                <i class="fa fa-handshake-o" aria-hidden="true"></i>
              </div>
              <h3>Made of Yours</h3>
              <p>Desain menggambarkan dirimu</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        <div class="row">
          <div class="col-md-12" style="padding-top:16px;margin-right:16px">
            <h3 class="ngedit"> Parallax Interactive</h3>
            <div id="work" class="parallaxTilt_hover">
              <img src="<?=$gambar.'fitur_bg.png'?>" class="tilt_bg" ></img>
              <img src="<?=$gambar.'fitur_share1.png'?>" class="tilt_share1" ></img>
              <img src="<?=$gambar.'fitur_human.png'?>" class="tilt_human" ></img>
              <img src="<?=$gambar.'fitur_share2.png'?>" class="tilt_share2" ></img>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="row">
    <div class=col-md-12>
    </div>
    <div class=col-md-6>
      <div class="tengah"></div>
    </div>
  </div>
</div>
</section>
<section class="price section-padding" data-scroll-index="6">
  <div class="container">
    <div class="row">
      <div class="section-head ">
        <h4><span class="textio"><div class="wow ctextio"></div>Our <i>Offers</i></span></h4>
      </div>
      <div class="text-center col-lg-12">
        <div class="row pricing-table">
          <div class="col-md-4">
            <div class=" item active mb-md50 back-border">
              <div class="value">
                <h3 class="harga"><span>Rp</span>10</h3>
                <span class="per">Per Monthly</span>

              </div>
              <div class="type">
                <h5>Basic</h5>
                <span></span>
              </div>
              <div class="features">
                <p><i class="fa fa-check" aria-hidden="true"></i>10 GB Disk Space</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>15 Domain Names</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>Enhanced Security</p>
                <p class="del"><i class="fa fa-check" aria-hidden="true"></i>Unlimited Support</p>
              </div>
              <div class="order">

              </div>
            </div>
          </div>
          <div class=" col-md-4">
            <div class="item active mb-sm50 back-border">
              <div class="value">
                <h3 class="harga"><span>Rp</span>30</h3>
                <span class="per">Per Monthly</span>
              </div>
              <div class="type">
                <h5>Standard</h5>
                <span></span>
              </div>
              <div class="features">
                <p><i class="fa fa-check" aria-hidden="true"></i>Full Access</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>100 GB Disk Space</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>30 Domain Names</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>Enhanced Security</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>Unlimited Support</p>
              </div>
              <div class="order">

              </div>
            </div>
          </div>
          <div class=" col-md-4">
            <div class="item active back-border">
              <div class="value">
                <h3 class="harga"><span>Rp</span>80</h3>
                <span class="per">Per Monthly</span>
              </div>
              <div class="type">
                <h5>Premium</h5>
                <span></span>
              </div>
              <div class="features">
                <p><i class="fa fa-check" aria-hidden="true"></i>500 GB Disk Space</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>100 Domain Names</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>Enhanced Security</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>Unlimited Support</p>
              </div>
              <div class="order">

              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
