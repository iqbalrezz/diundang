<style>
.sosmed-style{
  color:#ffffff;
  margin-right: 20px;
}
.sosmed-style a{
  color:#ffffff;

  padding: 15px 20px;

  font-size: 40px;
}.footer-copyright{
  color: #ffffff;
}
.sosmed-style a:hover{
  background: #23527c;
}
.footer-copyright{
  color: #ffffff;
}
.sosmed-style a:hover{
  background: #23527c;
}
.footer-copyright{
  color:#ffffff;
}
.sosmed-style a:hover{
  background: #23527c;
}
</style>
<div class="footer-menu">
  <div class="container">
    <div class="row">
      <div class="col-md-4">

        <h4 class="footer-title" >Menu</h4>
        <ul class="nav nav-pills nav-stacked">
          <li><a href="<?=base_url('home/')?>">Home</a></li>
          <li><a href="<?=base_url('galeri/detail')?>">Galeri</a></li>
          <li><a href="<?=base_url('pesan/tambah')?>">Pesan</a></li>
        </ul>
      </div>
      <div class="col-md-4">
        <h4 class="footer-title" >Tentang Diundang </h4>
        <ul class="nav nav-pills nav-stacked">
          <li><a href="about.html">Apa itu diundang.com</a></li>
          <li><a href="about.html">Panduan penggunaan</a></li>
          <li><a href="about.html">Kebijakan layanan</a></li>
          <li><a href="about.html">Kebijakan privasi</a></li>
          <li><a href="about.html">FAQ</a></li>
          <li><a href="about.html">partner bisnis</a></li>
        </ul>
      </div>
      <div class="col-md-4">
        <h4 class="footer-title" >follow Diundang</h4>
        <div class="nav nav-pills nav-stacked sosmed-style">
          <a href="" class="sosmed-style"><i class="fa fa-instagram"></i></a>
          <a href="" class="sosmed-style"><i class="fa fa-facebook"></i></a>
          <a href="" class="sosmed-style"><i class="fa fa-youtube"></i></a>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>
<footer class="footer-copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="copyright footer-copyright">
          Copyright <?=date("Y")?>  &copy; <?=$this->site_name?>.
        </p>
      </div>
    </div>
  </div>
</footer>
