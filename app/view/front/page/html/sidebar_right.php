<div class="row">
  <div class="col-md-12">

    <div class="row" style="margin-top: 2em;">
      <div class="col-md-12">
        <ul class="nav nav-pills nav-stacked nav-aing">
          <li><a href="<?=base_url('kategori/')?>">Semua Kategori</a></li>
          <?php if(isset($blogkategori_list)){ foreach($blogkategori_list as $bkl){ ?>
          <li><a href="<?=base_url('kategori/'.$bkl->slug)?>"><?=$bkl->nama?></a></li>
          <?php }} ?>
        </ul>
      </div>
    </div>

    <div class="row row-event">
      <div class="col-md-12">
        <h2>Event yang akan datang</h2>
      </div>
      <div class="col-md-12">
        <div class="event-slider">
          <?php if(isset($kegiatan_upcoming)){ foreach($kegiatan_upcoming as $kgu){ ?>
          <?php
          $date = explode(" ",$kgu->sdate);
          $dates = explode("-",$date[0]);
          $tgl = $this->__dateIndonesia($kgu->sdate,"tanggal");
          $tgls = explode(" ",$tgl);
          ?>
          <div class="event-slider-item" style="background-image: url('<?=$this->cdn_url($kgu->featured_image)?>')">
            <div class="event-slider-teks">
              <h5><a href="<?=base_url("kegiatan/".$dates[0]."/".$dates[1]."/$kgu->slug")?>"><?=$kgu->judul?></a></h5>
              <p><i class="fa fa-clock-o"></i> <?=$this->__dateIndonesia($kgu->sdate)?></p>
              <p><i class="fa fa-map-marker"></i> <?=$kgu->tempat?></p>
            </div>
          </div>
          <?php }} ?>
        </div>
      </div>
    </div>

  </div>
</div>
