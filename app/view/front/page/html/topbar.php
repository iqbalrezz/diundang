<div class="top-nav">
  <div class="container">
    <div class="row col-md-12">
      <div class="top-nav-text">
        <span class="topbar-tanggal"><?=$this->__dateIndonesia("now","hari_tanggal")?></span>
      </div>
      <div class="top-nav-icon-blocks">
        <?php if(strlen($this->site_social_email)>4){ ?>
        <div class="icon-block">
          <p><a target="_blank" href="mailto:<?=$this->site_social_email?>"><i class="fa fa-envelope-open-o"></i><span></span></a></p>
        </div>
        <?php } ?>
        <?php if(strlen($this->site_social_ig)>4){ ?>
        <div class="icon-block">
          <p><a href="<?=$this->site_social_ig?>"><i class="fa fa-instagram"></i><span></span></a></p>
        </div>
        <?php } ?>
        <?php if(strlen($this->site_social_fb)>4){ ?>
        <div class="icon-block">
          <p><a href="<?=$this->site_social_fb?>"><i class="fa fa-facebook"></i><span></span></a></p>
        </div>
        <?php } ?>
        <?php if(strlen($this->site_social_twitter)>4){ ?>
        <div class="icon-block">
          <p><a href="<?=$this->site_social_twitter?>"><i class="fa fa-twitter"></i><span></span></a></p>
        </div>
        <?php } ?>
        <?php if(strlen($this->site_social_youtube)>4){ ?>
        <div class="icon-block">
          <p><a href="<?=$this->site_social_youtube?>"><i class="fa fa-youtube"></i><span></span></a></p>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
