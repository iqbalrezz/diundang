<div class="row">
  <div class="col-md-8">
    <?php if(isset($post_per_kategori)){ foreach($post_per_kategori as $ppk){ ?>
    <?php if(strlen($ppk->featured_image)<=4) $ppk->featured_image = 'media/upload/default.jpg';?>
    <?php $kategories = explode(",",rtrim($ppk->kategori,",")); if(isset($kategories[0])) $ppk->kategori = $kategories[0]; ?>
    <!-- artikel baru -->
    <div class="row artikel-terbaru">
      <div class="col-md-6">
        <a href="<?=base_url("blog/$ppk->slug")?>" title="<?=$ppk->title?>">
          <div class="artikel-terbaru-featured-image" style="background-image: url('<?=$this->cdn_url($ppk->featured_image)?>')"></div>
        </a>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-12 sebelum-judul">
            <?php if(isset($blogkategori_list[$ppk->kategori])){ $kat = $blogkategori_list[$ppk->kategori];?>
            <span class="label label-danger"><a href="<?=base_url("kategori/".$kat->slug)?>"><?=$kat->nama?></a></span>
            <?php } ?>
            <span class="waktu"><i class="fa fa-clock-o"></i> <?=$this->__dateIndonesia($ppk->cdate)?></span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h5><a href="<?=base_url("blog/".$ppk->slug)?>" title="<?=$ppk->title?>"><?=$ppk->title?></a></h5>
            <p><?=$ppk->excerpt?></p>
          </div>
        </div>
      </div>
    </div>
    <!-- end artikel baru -->
    <?php }} ?>

  </div>
  <div class="col-md-4 konten-kanan">
    <?php $this->getThemeElement("page/html/sidebar_right",$__forward); ?>
  </div>
</div>
