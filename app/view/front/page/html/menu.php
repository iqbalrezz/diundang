<nav class="navbar navbar-default stuck navbar-transparent ">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=base_url()?>" title="<?=$this->site_name?>" >
        <?php if(strlen($this->site_logo)>4){ ?>
        <div class="seme-site-logo-wrapper">
          <img src="<?=$this->cdn_url($this->site_logo)?>" class="seme-site-logo" alt="<?=($this->site_title)?>" />
          <?=$this->site_name?>
        </div>
        <?php }else{ ?>
        <?=strtoupper($this->site_title)?>
        <?php } ?>
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li>

          <a href="<?=base_url('home/')?>"><i class="fa fa-home"></i> Home</a>
        </li>
        <li>
          <a href="<?=base_url('galeri/home')?>"><i class="fa fa-area-chart" aria-hidden="true"></i> Galeri</a>
        </li>
        <li>
          <a href="<?=base_url('pesan/tambah')?>"><i class="fa fa-envelope-open" aria-hidden="true"></i> Pesan</a>
        </li>
        <?php if(isset($menu_main)){ foreach($menu_main as $m1){ ?>
          <?php $url = $m1->url; if($m1->url_type == 'internal') $url = base_url($m1->url); ?>
          <?php if($m1->url == '#') $url = '#'; ?>
          <?php if(count($m1->childs)>0){ ?>
            <li class="dropdown" style="font-weight:700">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="<?=$m1->icon_class?>"></i>
                <?=$m1->nama?>
                <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu">
              <?php foreach($m1->childs as $m2){ ?>
                <?php $url = $m2->url; if($m2->url_type == 'internal') $url = base_url($m2->url); ?>
                <?php if($m2->url == '#') $url = '#'; ?>
                <?php if(count($m2->childs)>0){ ?>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <?=$m2->nama?> <i class="fa fa-angle-down"></i>
                  </a>
                  <ul class="dropdown-menu">
                    <?php foreach($m2->childs as $m3){ ?>
                    <?php $url = $m3->url; if($m3->url_type == 'internal') $url = base_url($m3->url); ?>
                    <?php if($m3->url == '#') $url = '#'; ?>
                    <li class=""><a href="<?=$url?>" title="Menuju <?=$m3->nama?>"><?=$m3->nama?></a></li>
                    <?php } ?>
                  </ul>
                </li>
                <?php }else{ ?>
                  <li class="" style="font-weight:700"><a href="<?=$url?>" title="Menuju <?=$m2->nama?>"><?=$m2->nama?></a></li>
                <?php } ?>
              <?php } ?>
              </ul>
            </li>
          <?php }else{ ?>
            <li style="font-weight:700"><a href="<?=$url?>" title="Menuju <?=$m1->nama?>" class="" ><i class="<?=$m1->icon_class?>" aria-hidden="true"></i> <?=$m1->nama?></a></li>
          <?php } ?>
        <?php }} ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
