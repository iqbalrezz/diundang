<?php
//Example 1 column layout
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->getThemeElement("page/html/head",$__forward); ?>
<?php $this->getBodyBefore(); ?>
<body class="">
  <?php $this->getThemeElement("page/html/topbar",$__forward);?>
  <?php $this->getThemeElement("page/html/menu",$__forward);?>
  <div class="container">
    <div class="row">
      <!-- main content-->
      <div class="col-md-8 konten-utama">
        <?php $this->getThemeContent(); ?>
      </div>
      <div class="col-md-4 konten-kanan">
        <?php $this->getThemeElement('page/html/sidebar_right',$__forward); ?>
      </div>
      <!-- main content-->
    </div>
  </div>

  <!--footer-->
  <?php $this->getThemeElement('page/html/footer',$__forward); ?>
  <!--end footer-->

  <!-- load JS in footer-->
  <?php $this->getJsFooter(); ?>
  <!-- End load JS in footer-->

  <!-- default JS Script-->
  <script>
  $(document).ready(function(e){
    $('.event-slider').slick({
      arrows : false,
      cssEase: 'linear',
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 1
    });
    <?php $this->getJsReady(); ?>
    <?php $this->getJsContent(); ?>
  });
  </script>
  <!-- default JS Script-->
</body>
</html>
