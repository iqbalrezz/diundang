<style>
.posisi-judul-pesan{
  margin-top: 70px;
}
.input-icon-kalender{
  padding-left: 30px;
}
</style>

<div class="row">
  <?php if(isset($notif)){ ?>
    <div class="alert alert-info">
      <p><?=$notif?></p>
    </div>
  <?php } ?>
  <div class="col-md-12 posisi-judul-pesan">
    <h1>Pesan Undanganmu</h1>
    <div class="row">
      <div class="col-md-3">

      </div>
      <div class="col-md-6">
        <form id="ipesan_form" action="<?=base_url('pesan/tambah/index')?>" method="post" class="form-horizontal">
          <div class="form-group">
            <div class="col-md-12">
              <label for="iname">Nama</label>
              <input id="iname" name="nama" class="form-control" value="" placeholder="Nama lengkap" />
            </div>
            <div class="col-md-12">
              <label for="ienohp">nohp</label>
              <input id="ienohp" name="no_telepon" class="form-control" value="" placeholder="nohp" />
            </div>
            <div class="col-md-12">
              <label for="ieemail">email</label>
              <input id="ieemail" name="email" class="form-control" value="" placeholder="email" />

            </div>
            <div class="col-md-12">
              <label id="gbijk" class="control_label">fitur</label>
              <select id="gbijk" name="jk" class="form-control">
                <option value="1" data-kode="1">basic</option>
                <option value="2" data-kode="2">standard</option>
                <option value="3" data-kode="3">premium</option>
              </select>
            </div>
          </div>
          <div class="form-action">
            <div class="col-md-12">
              <div class="btn-group pull-right">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> subnit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-3">
      </div>
    </div>
  </div>
</div>
