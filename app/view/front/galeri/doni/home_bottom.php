(function() {

//	default opts:
//	parallaxMultiple: 0.1,
//	tiltMultiple: 0.05,
//      duration: 500,
//      easing:'easeOutExpo'

    $('.par_satu').parallaxTilt({
      parallaxMultiple: .12
    });
    $('.par_dua').parallaxTilt({
      parallaxMultiple: .18
    });
    $('.par_tiga').parallaxTilt({
      parallaxMultiple: .10
    });
    $('.par_empat').parallaxTilt({
      parallaxMultiple: .05
    });
    $('.par_lima').parallaxTilt({
      parallaxMultiple: .12
    });
    $('.par_enam').parallaxTilt({
      parallaxMultiple: .18
    });
    $('.par_tujuh').parallaxTilt({
      parallaxMultiple: .10
    });
    $('.par_delapan').parallaxTilt({
      parallaxMultiple: .12
    });
    $('.par_sembilan').parallaxTilt({
      parallaxMultiple:  .07
    });

})();
