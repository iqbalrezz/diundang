<?php
class Home extends JI_Controller{

	public function __construct(){
    parent::__construct();
		$this->setTheme('front');
    $this->lib("seme_purifier");
	}

	protected function __getConfig(){
		$hpcache = new stdClass();
		$hpcache->mtitle = '';
		$hpcache->mdescription = '';
		$hpcache->mkeyword = '';
		$hpcache->slider_enable = '';
		$hpcache->slider_list = '';

		$homepage_cache = SENECACHE.'/homepage.json';
		if(file_exists($homepage_cache)){
			$fp = fopen($homepage_cache,'r');
			if(!$fp){
			}else{
				$json = fread($fp,filesize($homepage_cache));
				if(strlen($json)>3){
					$hpcache = json_decode($json);
				}
				fclose($fp);
			}
		}
		return $hpcache;
	}
	protected function __slugify($text){
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		return $text;
	}
	public function index(){
		$this->setTitle('Welcome to Diundang '.$this->site_suffix);
		$this->setDescription('Travtou is Travel and Touring platform grow your community to digital');
		$this->setKeyword('Travtou');

		$data["gambar"] = base_url("skin/front/images/");

		//main content
		//this view can be found on app/view/front/home/home.php
		$this->putThemeContent("galeri/detail",$data); //pass data to view
		//this view for INPAGE JS Script can be found on app/view/front/page/home/home_bottom.php
		$this->putJsContent("galeri/detail_bottom",$data); //pass data to view

		$this->loadLayout("col-1",$data);
		$this->render();
	}

}
