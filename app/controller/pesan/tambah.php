<?php
class Tambah extends JI_controller{

public function __construct(){
  parent::__construct();
  $this->setTheme('front');
  $this->load("d_pesan_model","dpm");
}
public function index($id=""){
  $data = $this->__init();
  $nama = $this->input->post("nama");
  $no_telepon = $this->input->post("no_telepon");
  $email = $this->input->post("email");
  if(empty($id)) $id = '';
  if(empty($nama)) $nama = '';
  if(empty($no_telepon)) $no_telepon = '';
  if(empty($email)) $email = '';
  if(strlen($nama)>1 && strlen($nama)>1){
    $di = array();
    $di['nama'] = $nama;
    $di['no_telepon'] = $no_telepon;
    $di['email'] = $email;
    $res = $this->dpm->insert($di);
    if($res){
      $data['notif'] = 'permintaan telah di kirim silahkan tunggu kabar dari kami';
  }
}
$data['brand'] = $this->site_name;
$data['page_current'] = 'pesan';
$this->setTitle('Tambah Data Pesan');
$this->setDescription('SEME Framework PHP MVC Framework with small footprint for your business.');
$this->setKeyword('SEME Framework');
$this->putThemecontent("pesan/home",$data);
$this->loadLayout("col-1",$data);
$this->render();
}
}
