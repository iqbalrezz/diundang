-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2019 at 10:54 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smpkp`
--

-- --------------------------------------------------------

--
-- Table structure for table `a_company`
--

CREATE TABLE `a_company` (
  `id` int(4) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `utype` enum('pusat','wilayah','cabang') NOT NULL DEFAULT 'cabang',
  `kode` varchar(24) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kabkota` varchar(255) NOT NULL,
  `provinsi` varchar(255) NOT NULL,
  `kodepos` varchar(12) NOT NULL,
  `telp` varchar(24) NOT NULL,
  `website` varchar(255) NOT NULL,
  `jenis_apotek` enum('Instalasi Farmasi','Apotek') NOT NULL DEFAULT 'Instalasi Farmasi',
  `header_teks` varchar(255) NOT NULL DEFAULT 'CALYSTA SKINCARE',
  `header_img` varchar(255) NOT NULL,
  `footer_teks` varchar(255) NOT NULL COMMENT 'footer struk',
  `buffer_stok` decimal(10,5) NOT NULL DEFAULT 1.50000 COMMENT 'buffer stok untuk po',
  `pajak_persentase` decimal(4,2) NOT NULL DEFAULT 0.00 COMMENT 'pajak persen',
  `pajak_status` int(1) NOT NULL DEFAULT 0 COMMENT 'pajak on =1 / off=0',
  `latitude` decimal(14,11) NOT NULL DEFAULT -6.91474400000,
  `longitude` decimal(14,11) NOT NULL DEFAULT 107.60981000000,
  `limit_penerimaan` int(11) NOT NULL DEFAULT 5 COMMENT 'PO Penerimaan limit hari',
  `is_terima_retur` int(1) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 1,
  `a_company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabel untuk pusat dan cabang';

-- --------------------------------------------------------

--
-- Table structure for table `a_config`
--

CREATE TABLE `a_config` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL COMMENT 'nama pengaturan',
  `pilihan` varchar(255) NOT NULL,
  `nilai_default` varchar(255) NOT NULL,
  `nilai` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='untuk menyimpan konfigurasi';

--
-- Dumping data for table `a_config`
--

INSERT INTO `a_config` (`id`, `nama`, `pilihan`, `nilai_default`, `nilai`) VALUES
(1, 'homepage_youtube_id', '', 'HW2WAqcJFiU', ''),
(2, 'homepage_sosmed_fb', '', 'shafiramuslimfashion', 'shafiramuslimfashion'),
(3, 'homepage_sosmed_ig', '', 'shafiramuslimfashion', 'shafiramuslimfashion'),
(4, 'homepage_sosmed_youtube', '', 'channel/UC1RvA9jHwDphXEEctTG4rOw', 'channel/UCkrLFCBthN8kp88v1p3DcVQ'),
(5, 'instagram_clid', '', '', '78e3d24a2d494256ad0dc0a454d0de9f'),
(6, 'instagram_clst', '', '', ' baeb17d0c3534cb19c55b7f1029b8518'),
(7, 'instagram_rdrc', '', '', 'https://shafira.thecloudalert.com/instagram/'),
(8, 'support_email', '', '', 'thecloudalertcom@gmail.com'),
(9, 'homepage_mtitle', '', '', 'Halaman Utama'),
(10, 'homepage_mkeyword', '', '', 'Syubbaanululuum'),
(11, 'homepage_mdescription', '', '', 'Syubbaanululuum merupakan situs web yang dikelola oleh Ma\'had syubbaanululuum Kabupaten Bandung, Jawa Barat.'),
(12, 'homepage_slider_enable', '', '', '1'),
(19, 'homepage_slider_list', '', '', 'media/upload/2019/01/travtou-slider-1.jpg'),
(20, 'homepage_block1_enable', '', '', '1'),
(23, 'homepage_block2_enable', '', '', '1'),
(25, 'homepage_block3_enable', '', '', '1'),
(26, 'homepage_block3_youtube', '', '', ''),
(27, 'homepage_block4_enable', '', '', '1'),
(28, 'homepage_block4_youtube', '', '', ''),
(29, 'homepage_block1_mode', '', '', '3'),
(30, 'homepage_block1_items', '', '', '[{\"url\":\"tag\\/best-deal\",\"image\":\"media\\/upload\\/2018\\/10\\/block-1-1.jpg\",\"caption\":\"Best Deal\"},{\"url\":\"tag\\/outerwear\",\"image\":\"media\\/upload\\/2018\\/10\\/block-1-2.jpg\",\"caption\":\"Outerwear\"},{\"url\":\"tag\\/khimar\\/\",\"image\":\"media\\/upload\\/2018\\/10\\/block-1-3.jpg\",\"caption\":\"Khimar\"}]'),
(32, 'homepage_block2_teks', '', '', '<h2 style=\"text-align:center\">DEDICATED FOR YOUX</h2>\n\n<h4 style=\"text-align:center\">Through elegant and exclusive designs, SHAFIRA always accomodates the needs of fashionable Muslim fashion for any occasions. Thus, our products are made exclusively in limited pieces. Shop sooner, own them faster.</h4>\n'),
(33, 'homepage_block3_items', '', '', '[{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-3-1.jpg\",\"caption\":\"\"},{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-3-2.jpg\",\"caption\":\"\"},{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-3-3.jpg\",\"caption\":\"\"},{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-3-4.jpg\",\"caption\":\"\"},{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-3-5.jpg\",\"caption\":\"\"}]'),
(34, 'homepage_block3_teks', '', '', '<p>BRAND NEW</p>\n\n<h2 style=\"text-align:left\">Women&#39;s Collections</h2>\n\n<p>Shop the latest trends in&nbsp;Islamic&nbsp;clothing. Stylish &amp; modest&nbsp;Islamic&nbsp;clothing for&nbsp;Women. Hijabs, dresses, cardigans, modest fashion &amp; more.</p>\n\n<p><a class=\"btn-shop-dress\" href=\"/tag/dress/\">Shop Dress</a> <a class=\"btn-shop-dress\" href=\"/kategori\">Shop All</a></p>\n'),
(35, 'homepage_block5_enable', '', '', '1'),
(36, 'homepage_block5_teks', '', '', '<p>BRAND NEW</p>\n\n<h2 style=\"text-align:left\">Men&#39;s Collections</h2>\n\n<p>Collection for Men. Quality and style are the features of the Menswear Collection of SHAFIRA. This line will be ideal for a modern, dynamic man, who is accustomed to The Best.</p>\n\n<p><a class=\"btn-shop-dress\" href=\"/tag/men\">Shop All Mens</a></p>\n'),
(37, 'homepage_block5_items', '', '', '[{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-5-1.jpg\",\"caption\":\"\"},{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-5-2.jpg\",\"caption\":\"\"},{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-5-3.jpg\",\"caption\":\"\"},{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-5-4.jpg\",\"caption\":\"\"},{\"url\":\"\",\"image\":\"media\\/upload\\/2018\\/10\\/block-5-5.jpg\",\"caption\":\"\"}]'),
(38, 'homepage_block6_enable', '', '', '1'),
(39, 'homepage_block6_teks', '', '', '<p class=\"subtitle\">Local Event</p>\n          <h2 class=\"custom-font\" style=\"text-align:left;\">Indonesia Fashion Week 2018</h2>\n          <p>Alhamdulillah, thank you so much for the endless support. We made it into one of the big parts of our&nbsp;<a href=\"https://www.instagram.com/explore/tags/shafirajourney/\" class=\"text-link\">#SHAFIRAJourney</a>.</p>\n          <a href=\"#\" class=\"btn-shop-dress\">View Show</a>'),
(40, 'homepage_block6_image', '', '', 'media/upload/2018/10/block-6.jpg'),
(41, 'homepage_block4_teks', '', '', '<h1>Journey to The World Class Fashion</h1>\n								<h3>In Partnership with SWAROVSKI X</h3>\n'),
(42, 'homepage_block4_youtube_id', '', '', 'HW2WAqcJFiU'),
(43, 'homepage_block1_layout', '', '', ''),
(44, 'homepage_block1_list', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `a_grup`
--

CREATE TABLE `a_grup` (
  `id` int(11) NOT NULL,
  `kode` varchar(25) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `hakakses` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `a_jabatan`
--

CREATE TABLE `a_jabatan` (
  `id` int(11) NOT NULL,
  `a_company_id` int(11) DEFAULT NULL,
  `kode` varchar(16) NOT NULL DEFAULT '00',
  `utype` enum('pusat','wilayah','cabang') NOT NULL DEFAULT 'pusat',
  `jenis_shift` enum('office_hour','2day_ad','2day_wd','3day_ad','3day_wd') NOT NULL DEFAULT 'office_hour',
  `nama` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `item_gaji_jml` int(3) NOT NULL DEFAULT 0,
  `is_active` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `a_media`
--

CREATE TABLE `a_media` (
  `id` int(11) NOT NULL,
  `b_user_id` int(11) DEFAULT NULL,
  `utype` enum('blog','produk','produk_thumb','galeri') NOT NULL DEFAULT 'blog',
  `nama` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL DEFAULT '/',
  `cdate` datetime NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `a_media`
--

INSERT INTO `a_media` (`id`, `b_user_id`, `utype`, `nama`, `folder`, `cdate`, `is_active`) VALUES
(3, 1, 'blog', 'media/upload/2018/11/burung-1.jpg', '/test', '2018-11-30 18:56:28', 1),
(6, 1, 'blog', 'media/upload/2018/12/tenda-promosi-3x3-besi-pipa-210.png', '/tenda-promosi', '2018-12-19 15:42:54', 1),
(7, 1, 'blog', 'media/upload/2018/12/tenda-promosi-2x2.png', '/tenda-promosi', '2018-12-19 16:28:31', 1),
(8, 1, 'blog', 'media/upload/2018/12/tenda-promosi-4x4.png', '/tenda-promosi', '2018-12-19 16:33:45', 1),
(10, 1, 'blog', 'media/upload/2018/12/velbed-cover.jpg', '/velbed', '2018-12-19 20:00:30', 1),
(13, 1, 'blog', 'media/upload/2018/12/tenda-sarnafil-size-chart.png', '/tenda-sarnafil', '2018-12-19 20:57:17', 1),
(14, 1, 'blog', 'media/upload/2018/12/tenda-kerucut-3x3-ani.gif', '/tenda-kerucut', '2018-12-19 21:06:59', 1),
(15, 1, 'blog', 'media/upload/2019/01/travtou-slider-1.jpg', '/', '2019-01-07 21:59:08', 1),
(19, 1, 'blog', 'media/upload/2019/01/glamping-img.jpg', '/', '2019-01-09 16:44:45', 1),
(20, 1, 'blog', 'media/upload/2019/01/glamping-cover.jpg', '/', '2019-01-09 16:54:09', 1),
(21, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-001.jpg', '/glamping garut', '2019-02-02 22:01:57', 1),
(22, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-002.jpg', '/glamping garut', '2019-02-02 22:02:19', 1),
(23, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-003.jpg', '/glamping garut', '2019-02-02 22:02:40', 1),
(24, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-004.jpg', '/glamping garut', '2019-02-02 22:02:56', 1),
(25, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-005.jpg', '/glamping garut', '2019-02-02 22:03:11', 1),
(26, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-006.jpg', '/glamping garut', '2019-02-02 22:29:46', 1),
(27, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-007.jpg', '/glamping garut', '2019-02-02 22:30:06', 1),
(28, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-008.jpg', '/glamping garut', '2019-02-02 22:30:22', 1),
(29, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-009.jpg', '/glamping garut', '2019-02-02 22:30:38', 1),
(30, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-010.jpg', '/glamping garut', '2019-02-02 22:30:52', 1),
(31, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-011.jpg', '/glamping garut', '2019-02-02 22:31:08', 1),
(32, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-011.jpg', '/glamping garut', '2019-02-02 22:31:22', 1),
(33, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-012.jpg', '/glamping garut', '2019-02-02 22:31:41', 1),
(34, 1, 'blog', 'media/upload/2019/02/idjbgr.gc.pndy0001-013.jpg', '/glamping garut', '2019-02-02 22:31:57', 1),
(35, 1, 'blog', 'media/upload/2019/05/mekkah.jpg', '/', '2019-05-17 15:29:28', 1),
(36, 1, 'blog', 'media/upload/2019/05/bagi-bagi.png', '/', '2019-05-18 13:45:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `a_modules`
--

CREATE TABLE `a_modules` (
  `identifier` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT '',
  `level` int(1) NOT NULL DEFAULT 0 COMMENT 'depth level of menu, 0 mean outer 3 deeper submenu',
  `has_submenu` int(1) NOT NULL DEFAULT 0 COMMENT '1 mempunyai submenu, 2 tidak mempunyai submenu',
  `children_identifier` varchar(255) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1,
  `is_default` enum('allowed','denied') NOT NULL DEFAULT 'denied',
  `is_visible` int(1) NOT NULL DEFAULT 1,
  `priority` int(3) NOT NULL DEFAULT 0 COMMENT '0 mean higher 999 lower',
  `fa_icon` varchar(255) NOT NULL DEFAULT 'fa fa-home' COMMENT 'font-awesome icon on menu',
  `utype` varchar(48) NOT NULL DEFAULT 'internal' COMMENT 'type module : internal, external'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='list modul yang ada dimenu atau tidak ada dimenu';

--
-- Dumping data for table `a_modules`
--

INSERT INTO `a_modules` (`identifier`, `name`, `path`, `level`, `has_submenu`, `children_identifier`, `is_active`, `is_default`, `is_visible`, `priority`, `fa_icon`, `utype`) VALUES
('akun', 'Akun', 'akun', 0, 1, NULL, 1, 'denied', 1, 4, 'fa fa-users', 'internal'),
('akun_grup', 'Grup', 'akun/grup', 1, 0, 'akun', 1, 'denied', 0, 0, 'fa fa-home', 'internal'),
('akun_hak_akses', 'Hak Akses Pengguna', 'akun/hak_akses', 1, 0, 'akun', 1, 'denied', 0, 3, 'fa fa-edit', 'internal'),
('akun_karyawan', 'Karyawan', 'akun/karyawan', 1, 0, 'akun', 1, 'denied', 0, 2, 'fa fa-home', 'internal'),
('akun_pelanggan', 'Pelanggan', 'akun/pelanggan', 1, 0, 'akun', 1, 'denied', 0, 20, 'fa fa-home', 'internal'),
('akun_pengguna', 'Super Admin', 'akun/pengguna', 1, 0, 'akun', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('akun_usergroup', 'Grup Pelanggan', 'akun/usergroup', 1, 0, 'akun', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('alamatongkir', 'Alamat &amp; Ongkir', '#', 0, 1, NULL, 1, 'denied', 0, 1, 'fa fa-building', 'internal'),
('alamatongkir_jne', 'JNE', 'alamatongkir/jne', 1, 0, 'alamatongkir', 1, 'denied', 1, 71, 'fa fa-home', 'internal'),
('alamatongkir_jnt', 'JNT', 'alamatongkir/jnt', 1, 0, 'alamatongkir', 1, 'denied', 0, 72, 'fa fa-home', 'internal'),
('alamatongkir_kabkota', 'Kabkota', 'alamatongkir/kabkota', 1, 0, 'alamatongkir', 1, 'denied', 1, 52, 'fa fa-home', 'internal'),
('alamatongkir_kecamatan', 'Kecamatan', 'alamatongkir/kecamatan', 1, 0, 'alamatongkir', 1, 'denied', 1, 53, 'fa fa-home', 'internal'),
('alamatongkir_kelurahan', 'Kelurahan', 'alamatongkir/kelurahan', 1, 0, 'alamatongkir', 1, 'denied', 0, 54, 'fa fa-home', 'internal'),
('alamatongkir_negara', 'Negara', 'alamatongkir/negara', 1, 0, 'alamatongkir', 1, 'denied', 1, 50, 'fa fa-home', 'internal'),
('alamatongkir_posems', 'POS EMS', 'alamatongkir/posems', 1, 0, 'alamatongkir', 1, 'denied', 0, 74, 'fa fa-home', 'internal'),
('alamatongkir_poskilat', 'POS Kilat', 'alamatongkir/poskilat', 1, 0, 'alamatongkir', 1, 'denied', 0, 73, 'fa fa-home', 'internal'),
('alamatongkir_provinsi', 'Provinsi', 'alamatongkir/provinsi', 1, 0, 'alamatongkir', 1, 'denied', 1, 51, 'fa fa-home', 'internal'),
('blog', 'Blog', 'blog', 0, 0, NULL, 1, 'denied', 1, 3, 'fa fa-file-text-o', 'internal'),
('blog_kategori', 'Kategori', 'blog/kategori', 1, 0, 'blog', 1, 'denied', 1, 12, 'fa fa-home', 'internal'),
('blog_post', 'Post', 'blog/post', 1, 0, 'blog', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('cabang', 'Cabang', '#', 0, 1, NULL, 1, 'denied', 0, 4, 'fa fa-user-secret', 'internal'),
('cabang_barang', 'Barang per Cabang', 'cabang/barang', 1, 0, 'cabang', 1, 'denied', 1, 20, 'fa fa-home', 'internal'),
('cabang_detail', 'Detail', 'cabang/detail', 1, 0, 'cabang', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('cabang_pengumuman', 'Pengumuman', 'cabang/pengumuman', 1, 0, 'erpmaster', 1, 'denied', 1, 84, 'fa fa-home', 'internal'),
('cms', 'CMS', 'cms', 0, 1, NULL, 1, 'denied', 1, 12, 'fa fa-file-text-o', 'internal'),
('cms_blog', 'Blog', 'cms/blog', 1, 0, 'cms', 1, 'denied', 1, 21, 'fa fa-home', 'internal'),
('cms_homepage', 'Homepage', 'cms/homepage', 1, 0, 'cms', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('cms_media', 'Media', 'cms/media', 1, 0, 'cms', 1, 'denied', 1, 20, 'fa fa-home', 'internal'),
('cms_menu', 'Menu', 'cms/menu', 1, 0, 'cms', 1, 'denied', 1, 11, 'fa fa-home', 'internal'),
('cms_portofolio', 'Portofolio', 'cms/portofolio', 1, 0, 'cms', 1, 'denied', 1, 30, 'fa fa-home', 'internal'),
('cms_slider', 'Slider', 'cms/slider', 1, 0, 'cms', 1, 'denied', 1, 12, 'fa fa-home', 'internal'),
('cms_testimonial', 'Testimonial', 'cms/testimonial', 1, 0, 'cms', 1, 'denied', 1, 22, 'fa fa-home', 'internal'),
('crm', 'Data', 'crm', 0, 1, NULL, 1, 'denied', 1, 7, 'fa fa-book', 'internal'),
('crm_booking', 'Booking', 'crm/booking', 1, 0, 'crm', 1, 'denied', 0, 1, 'fa fa-home', 'internal'),
('crm_cs', 'CS', 'crm/cs', 1, 0, 'crm', 1, 'denied', 0, 3, 'fa fa-home', 'internal'),
('crm_datasiswabaru', 'Data Siswa Baru', 'crm/datasiswabaru', 1, 0, 'crm', 1, 'denied', 1, 22, 'fa fa-home', 'internal'),
('crm_kategori', 'Kategori', 'crm/kategori', 1, 0, 'crm', 1, 'denied', 0, 1, 'fa fa-home', 'internal'),
('crm_komplain', 'Komplain', 'crm/komplain', 1, 0, 'crm', 1, 'denied', 0, 5, 'fa fa-home', 'internal'),
('crm_konsultasi', 'Konsultasi', 'crm/konsultasi', 1, 0, 'crm', 1, 'denied', 0, 0, 'fa fa-home', 'internal'),
('crm_pemberitahuan', 'Pemberitahuan', 'crm/pemberitahuan', 1, 0, 'crm', 1, 'denied', 0, 3, 'fa fa-home', 'internal'),
('crm_pesan', 'Pesan', 'crm/pesan', 1, 0, 'crm', 1, 'denied', 0, 0, 'fa fa-home', 'internal'),
('crm_retur', 'Retur', 'crm/retur', 1, 0, 'crm', 1, 'denied', 0, 4, 'fa fa-home', 'internal'),
('crm_support', 'Support', 'crm/support', 1, 0, 'crm', 1, 'denied', 0, 2, 'fa fa-home', 'internal'),
('dashboard', 'Dashboard', '', 0, 0, NULL, 1, 'allowed', 1, 0, 'fa fa-home', 'internal'),
('ecommerce', 'Ecommerce', 'ecommerce', 0, 1, NULL, 1, 'denied', 1, 10, 'fa fa-shopping-cart', 'internal'),
('ecommerce_bank', 'Bank', 'ecommerce/bank', 1, 0, 'ecommerce', 1, 'denied', 0, 80, 'fa fa-home', 'internal'),
('ecommerce_bantuan', 'Bantuan (CS)', 'ecommerce/bantuan', 1, 0, 'ecommerce', 1, 'denied', 0, 60, 'fa fa-home', 'internal'),
('ecommerce_custom', 'Custom', 'ecommerce/custom', 1, 0, 'ecommerce', 1, 'denied', 0, 20, 'fa fa-home', 'internal'),
('ecommerce_homepage', 'Homepage', 'ecommerce/homepage', 1, 1, 'ecommerce', 1, 'denied', 0, 50, 'fa fa-home', 'internal'),
('ecommerce_homepage_produk', 'Produk', 'ecommerce/homepage/produk', 2, 0, 'ecommerce_homepage', 1, 'denied', 1, 52, 'fa fa-home', 'internal'),
('ecommerce_homepage_slider', 'Slider', 'ecommerce/homepage/slider', 2, 0, 'ecommerce_homepage', 1, 'denied', 1, 52, 'fa fa-home', 'internal'),
('ecommerce_kategori', 'Kategori', 'ecommerce/kategori', 1, 0, 'ecommerce', 1, 'denied', 1, 20, 'fa fa-home', 'internal'),
('ecommerce_menu', 'Menu', 'ecommerce/menu', 1, 0, 'ecommerce', 1, 'denied', 0, 50, 'fa fa-home', 'internal'),
('ecommerce_order', 'Order', 'ecommerce/order', 1, 0, 'ecommerce', 1, 'denied', 0, 10, 'fa fa-home', 'internal'),
('ecommerce_pelanggan', 'Pelanggan', 'ecommerce/pelanggan', 1, 0, 'ecommerce', 1, 'denied', 1, 60, 'fa fa-home', 'internal'),
('ecommerce_pembayaran', 'Cara Bayar', 'ecommerce/pembayaran', 1, 0, 'ecommerce', 1, 'denied', 0, 81, 'fa fa-home', 'internal'),
('ecommerce_pengaturan', 'Pengaturan', 'ecommerce/pengaturan', 1, 0, 'ecommerce', 1, 'denied', 0, 90, 'fa fa-home', 'internal'),
('ecommerce_pengiriman', 'Pengiriman', 'ecommerce/pengiriman', 1, 0, 'ecommerce', 1, 'denied', 0, 45, 'fa fa-home', 'internal'),
('ecommerce_produk', 'Produk', 'ecommerce/produk', 1, 0, 'ecommerce', 1, 'denied', 1, 25, 'fa fa-home', 'internal'),
('ecommerce_promo', 'Promo', 'ecommerce/promo', 1, 0, 'ecommerce', 1, 'denied', 0, 56, 'fa fa-home', 'internal'),
('ecommerce_slider', 'Slider', 'ecommerce/slider', 1, 0, 'ecommerce', 1, 'denied', 1, 55, 'fa fa-home', 'internal'),
('erpmaster', 'ERP Master', '#', 0, 1, NULL, 1, 'denied', 0, 1, 'fa fa-building', 'internal'),
('erpmaster_bank', 'Bank', 'erpmaster/bank', 1, 0, 'erpmaster', 1, 'denied', 1, 60, 'fa fa-home', 'internal'),
('erpmaster_brand', 'Brand', 'erpmaster/brand', 1, 0, 'erpmaster', 1, 'denied', 0, 41, 'fa fa-home', 'internal'),
('erpmaster_company', 'Cabang', 'erpmaster/company', 1, 0, 'erpmaster', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('erpmaster_gudang', 'Gudang', 'erpmaster/gudang', 1, 0, 'erpmaster', 1, 'denied', 1, 12, 'fa fa-home', 'internal'),
('erpmaster_jabatan', 'Jabatan', 'erpmaster/jabatan', 1, 0, 'erpmaster', 1, 'denied', 1, 11, 'fa fa-home', 'internal'),
('erpmaster_pembayaran', 'Cara Bayar', 'erpmaster/pembayaran', 1, 0, 'erpmaster', 1, 'denied', 0, 61, 'fa fa-home', 'internal'),
('erpmaster_toko', 'Toko', 'erpmaster/toko', 1, 0, 'erpmaster', 1, 'denied', 0, 41, 'fa fa-home', 'internal'),
('erpmaster_vendor', 'Supplier', 'erpmaster/vendor', 1, 0, 'erpmaster', 1, 'denied', 1, 40, 'fa fa-home', 'internal'),
('event', 'Event', '#', 0, 0, NULL, 1, 'denied', 1, 4, 'fa fa-calendar', 'internal'),
('event_kajian', 'Kajian', 'event/kajian', 1, 0, 'event', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('event_kegiatan', 'Kegiatan', 'event/kegiatan', 1, 0, 'event', 1, 'denied', 1, 2, 'fa fa-home', 'internal'),
('gudang', 'Gudang', '#', 0, 1, NULL, 1, 'denied', 0, 8, 'fa fa-gift', 'internal'),
('gudang_bahanbaku', 'Bahan Baku', 'gudang/bb', 1, 0, 'gudang', 1, 'denied', 1, 70, 'fa fa-home', 'internal'),
('gudang_pengajuan', 'Pengajuan', 'gudang/pengajuan', 1, 0, 'gudang', 1, 'denied', 1, 60, 'fa fa-home', 'internal'),
('gudang_pengiriman', 'Pengiriman Barang', 'gudang/pengiriman', 1, 0, 'gudang', 1, 'denied', 1, 12, 'fa fa-home', 'internal'),
('gudang_permintaan', 'Permintaan Barang', 'gudang/permintaan', 1, 0, 'gudang', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('gudang_persetujuan', 'Persetujuan Permintaan', 'gudang/persetujuan', 1, 0, 'gudang', 1, 'denied', 1, 11, 'fa fa-home', 'internal'),
('gudang_pindah', 'Pindah Barang', 'gudang/pindah', 1, 0, 'gudang', 1, 'denied', 1, 30, 'fa fa-home', 'internal'),
('gudang_produksi', 'Barang Produksi', 'gudang/produksi', 1, 0, 'gudang', 1, 'denied', 1, 31, 'fa fa-home', 'internal'),
('gudang_retur', 'Barang Retur', 'gudang/retur', 1, 0, 'gudang', 1, 'denied', 1, 32, 'fa fa-home', 'internal'),
('gudang_so', 'Stok Opname', 'gudang/so', 1, 0, 'gudang', 1, 'denied', 1, 33, 'fa fa-home', 'internal'),
('gudang_stok', 'Penyimpanan &amp; Stok', 'gudang/stok', 1, 0, 'gudang', 1, 'denied', 1, 34, 'fa fa-home', 'internal'),
('hr', 'HR', '#', 0, 1, NULL, 1, 'denied', 0, 5, 'fa fa-user', 'internal'),
('keuangan', 'Keuangan', 'keuangan', 0, 1, NULL, 1, 'denied', 0, 6, 'fa fa-money', 'internal'),
('keuangan_deposit', 'Deposit', 'keuangan/deposit', 1, 0, 'keuangan', 1, 'denied', 1, 6, 'fa fa-home', 'internal'),
('keuangan_inventaris', 'Inventaris', 'keuangan/inventaris', 1, 0, 'keuangan', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('keuangan_kas', 'Kas', 'keuangan/kas', 1, 0, 'keuangan', 1, 'denied', 1, 2, 'fa fa-home', 'internal'),
('keuangan_laporan', 'Laporan', 'keuangan/laporan', 1, 0, 'keuangan', 1, 'denied', 1, 12, 'fa fa-home', 'internal'),
('keuangan_memo', 'Kredit Memo', 'keuangan/memo', 1, 0, 'keuangan', 1, 'denied', 1, 7, 'fa fa-home', 'internal'),
('keuangan_pengajuan', 'Pengajuan', 'keuangan/pengajuan', 1, 0, 'keuangan', 1, 'denied', 1, 4, 'fa fa-home', 'internal'),
('keuangan_perjalanan', 'Perjalanan', 'keuangan/perjalanan', 1, 0, 'keuangan', 1, 'denied', 1, 3, 'fa fa-home', 'internal'),
('keuangan_rekening', 'Rekening', 'keuangan/rekening', 1, 0, 'keuangan', 1, 'denied', 1, 8, 'fa fa-home', 'internal'),
('klinik', 'Klinik', '#', 0, 1, NULL, 1, 'denied', 0, 5, 'fa fa-asterisk', 'internal'),
('klinik_rekammedis', 'Rekam Medis', 'klinik/rekammedis', 1, 0, 'klinik', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('laporan', 'Laporan', 'laporan', 0, 1, NULL, 1, 'denied', 0, 30, 'fa fa-file-text', 'internal'),
('laporan_asistensi', 'Asistensi', 'laporan/asistensi', 1, 0, 'laporan', 1, 'denied', 1, 11, 'fa fa-home', 'internal'),
('laporan_barang', 'Barang', 'laporan/barang', 1, 0, 'laporan', 1, 'denied', 0, 5, 'fa fa-home', 'internal'),
('laporan_bestseller', 'Best Seller', 'laporan/bestseller', 1, 0, 'laporan', 1, 'denied', 1, 24, 'fa fa-home', 'internal'),
('laporan_booking', 'Booking', 'laporan/booking', 1, 0, 'laporan', 0, 'denied', 0, 3, 'fa fa-home', 'internal'),
('laporan_kasir', 'Kasir', 'laporan/kasir', 1, 0, 'laporan', 1, 'denied', 1, 7, 'fa fa-home', 'internal'),
('laporan_pelanggan', 'Pelanggan', 'laporan/pelanggan', 1, 0, 'laporan', 1, 'denied', 1, 2, 'fa fa-home', 'internal'),
('laporan_penjualan', 'Penjualan', 'laporan/penjualan', 1, 0, 'laporan', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('laporan_terapis', 'Terapis', 'laporan/terapis', 1, 0, 'laporan', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('pembelian', 'Pembelian', 'pembelian', 0, 1, NULL, 1, 'denied', 0, 5, 'fa fa-opencart', 'internal'),
('pembelian_barang', 'Pembelian Barang', 'pembelian/barang', 1, 0, 'pembelian', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('pembelian_historystok', 'History Stok', 'pembelian/historystok', 1, 0, 'pembelian', 1, 'denied', 1, 3, 'fa fa-home', 'internal'),
('pembelian_karantina', 'Karantina Batch', 'pembelian/karantina', 1, 0, 'pembelian', 1, 'denied', 1, 5, 'fa fa-home', 'internal'),
('pembelian_mutasi', 'Mutasi', 'pembelian/mutasi', 1, 0, 'pembelian', 1, 'denied', 1, 4, 'fa fa-home', 'internal'),
('pembelian_orderan', 'Orderan', 'pembelian/orderan', 1, 0, 'pembelian', 1, 'denied', 0, 8, 'fa fa-home', 'internal'),
('pembelian_produksi', 'Produksi', 'pembelian/produksi', 1, 0, 'pembelian', 1, 'denied', 0, 9, 'fa fa-home', 'internal'),
('pembelian_racikan', 'Proses Racikan', 'pembelian/racikan', 1, 0, 'pembelian', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('pengiriman', 'Pengiriman', 'pengiriman', 0, 1, NULL, 1, 'denied', 0, 5, 'fa fa-truck', 'internal'),
('pengiriman_ekspedisi', 'Ekspedisi', 'pengriman/ekspedisi', 1, 0, 'pengiriman', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('pengiriman_kurir', 'Kurir', 'pengiriman/kurir', 1, 0, 'pengiriman', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('pengiriman_packing', 'Packing', 'pengiriman/packing', 1, 0, 'pengiriman', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('pengiriman_qc', 'QC', 'pengiriman/qc', 1, 0, 'pengiriman', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('penjualan', 'Penjualan', 'penjualan', 0, 1, NULL, 1, 'denied', 0, 9, 'fa fa-book', 'internal'),
('penjualan_bonus', 'Bonus', 'penjualan/bonus', 1, 0, 'penjualan', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('penjualan_cabang', 'Cabang', 'penjualan/cabang', 0, 0, 'penjualan', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('penjualan_offline', 'Offline', 'penjualan/offline', 1, 0, 'penjualan', 0, 'denied', 0, 20, 'fa fa-home', 'internal'),
('penjualan_online', 'Online', 'penjualan/online', 1, 0, 'penjualan', 1, 'denied', 0, 30, 'fa fa-home', 'internal'),
('penjualan_outlet', 'Outlet (offline)', 'penjualan/outlet', 1, 0, 'penjualan', 1, 'denied', 0, 21, 'fa fa-home', 'internal'),
('penjualan_promosi', 'Promosi', 'penjualan/promosi', 1, 0, 'penjualan', 1, 'denied', 1, 6, 'fa fa-home', 'internal'),
('penjualan_specialprice', 'Special Price', 'penjualan/specialprice', 1, 0, 'penjualan', 1, 'denied', 1, 4, 'fa fa-home', 'internal'),
('penjualan_voucher', 'Voucher', 'penjualan/voucher', 1, 0, 'penjualan', 1, 'denied', 1, 6, 'fa fa-home', 'internal'),
('po', 'P.O.', '#', 0, 1, NULL, 1, 'denied', 0, 16, 'fa fa-dropbox', 'internal'),
('po_faktur', 'Faktur', 'po/faktur', 1, 0, 'po', 1, 'denied', 1, 22, 'fa fa-home', 'internal'),
('po_konfirmasipesanan', 'Konfirmasi Pesanan', 'po/konfirmasipesanan', 1, 0, 'po', 1, 'denied', 1, 2, 'fa fa-home', 'internal'),
('po_outstanding', 'Outstanding', 'po/outstanding', 1, 0, 'po', 1, 'denied', 1, 42, 'fa fa-home', 'internal'),
('po_penerimaan', 'Penerimaan', 'po/penerimaan', 1, 0, 'po', 1, 'denied', 1, 30, 'fa fa-home', 'internal'),
('po_pengiriman', 'Pengiriman', 'po/pengiriman', 1, 0, 'po', 1, 'denied', 1, 20, 'fa fa-home', 'internal'),
('po_permintaan', 'Permintaan', 'po/permintaan', 1, 0, 'po', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('po_persetujuan', 'Persetujuan', 'po/persetujuan', 1, 0, 'po', 0, 'denied', 0, 10, 'fa fa-home', 'internal'),
('po_selisih', 'Selisih', 'po/selisih', 1, 0, 'po', 1, 'denied', 1, 40, 'fa fa-home', 'internal'),
('po_suratjalan', 'Surat Jalan', 'po/suratjalan', 1, 0, 'po', 1, 'denied', 0, 21, 'fa fa-home', 'internal'),
('po_suratpenerimaan', 'Surat Penerimaan', 'po/suratpenerimaan', 1, 0, 'po', 1, 'denied', 1, 32, 'fa fa-home', 'internal'),
('po_suratpesanan', 'Surat Pesanan', 'po/suratpesanan', 1, 0, 'po', 1, 'denied', 1, 3, 'fa fa-home', 'internal'),
('po_transit', 'Transit Pengiriman', 'po/transit', 1, 0, 'po', 1, 'denied', 1, 22, 'fa fa-home', 'internal'),
('po_verifikasipenerimaan', 'Verifikasi Penerimaan', 'po/verifikasipenerimaan', 1, 0, 'po', 1, 'denied', 1, 31, 'fa fa-home', 'internal'),
('produk', 'Produk Master', '#', 0, 1, NULL, 1, 'denied', 0, 3, 'fa fa-building', 'internal'),
('produksi', 'Produksi', 'produksi', 0, 1, NULL, 1, 'denied', 0, 6, 'fa fa-cogs', 'internal'),
('produksi_pekerjaan', 'Pekerjaan', 'produksi/pekerjaan', 1, 0, 'produksi', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('produksi_proses', 'Proses', 'produksi/proses', 1, 0, 'produksi', 1, 'denied', 1, 3, 'fa fa-home', 'internal'),
('produksi_rencana', 'Rencana', 'produksi/rencana', 1, 0, 'produksi', 1, 'denied', 1, 2, 'fa fa-home', 'internal'),
('produk_barang', 'Barang', 'produk/barang', 1, 0, 'produk', 1, 'denied', 1, 15, 'fa fa-home', 'internal'),
('produk_grup', 'Grup Barang', 'produk/grup', 1, 0, 'produk', 1, 'denied', 0, 3, 'fa fa-home', 'internal'),
('produk_jasa', 'Jasa', 'produk/jasa', 1, 0, 'produk', 1, 'denied', 1, 16, 'fa fa-home', 'internal'),
('produk_jenis', 'Grup Jenis', 'produk/jenis', 1, 0, 'produk', 1, 'denied', 1, 5, 'fa fa-home', 'internal'),
('produk_kategori', 'Kategori Produk', 'produk/kategori', 1, 0, 'produk', 1, 'denied', 1, 10, 'fa fa-home', 'internal'),
('produk_meta', 'Meta', 'produk/meta', 1, 0, 'produk', 1, 'denied', 0, 11, 'fa fa-home', 'internal'),
('produk_paket', 'Paket', 'produk/paket', 1, 0, 'produk', 1, 'denied', 1, 20, 'fa fa-home', 'internal'),
('produk_produk', 'Produk', 'produk/produk', 1, 0, 'produk', 1, 'denied', 0, 12, 'fa fa-home', 'internal'),
('produk_racikan', 'Racikan', 'produk/racikan', 1, 0, 'produk', 1, 'denied', 1, 20, 'fa fa-home', 'internal'),
('produk_satuan', 'Satuan/Unit', 'produk/satuan', 1, 0, 'produk', 1, 'denied', 1, 4, 'fa fa-home', 'internal'),
('retur', 'retur', '#', 0, 1, NULL, 1, 'denied', 0, 16, 'fa fa-sign-out', 'internal'),
('retur_barang', 'Barang', 'retur/barang', 1, 0, 'retur', 0, 'denied', 0, 1, 'fa fa-home', 'internal'),
('retur_kirim', 'Pengiriman', 'retur/kirim', 1, 0, 'retur', 1, 'denied', 1, 1, 'fa fa-home', 'internal'),
('retur_mutasi', 'Mutasi', 'retur/mutasi', 1, 0, 'retur', 0, 'denied', 0, 11, 'fa fa-home', 'internal'),
('retur_recycle', 'Daur Ulang', 'retur/recycle', 1, 0, 'retur', 1, 'denied', 1, 20, 'fa fa-home', 'internal'),
('retur_selisih', 'Selisih', 'retur/selisih', 1, 0, 'retur', 1, 'denied', 1, 6, 'fa fa-home', 'internal'),
('retur_terima', 'Penerimaan', 'retur/terima', 1, 0, 'retur', 1, 'denied', 1, 2, 'fa fa-home', 'internal'),
('retur_transit', 'Transit Pengiriman', 'retur/transit', 1, 0, 'retur', 1, 'denied', 1, 3, 'fa fa-home', 'internal');

-- --------------------------------------------------------

--
-- Table structure for table `a_pengguna`
--

CREATE TABLE `a_pengguna` (
  `id` int(4) NOT NULL,
  `a_company_id` int(5) DEFAULT NULL COMMENT 'penempatan',
  `a_company_nama` varchar(255) NOT NULL DEFAULT '-',
  `a_company_kode` varchar(32) NOT NULL DEFAULT '-',
  `a_jabatan_id` int(11) DEFAULT NULL,
  `a_jabatan_nama` varchar(255) NOT NULL DEFAULT 'Staff',
  `username` varchar(24) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `welcome_message` varchar(255) NOT NULL,
  `scope` enum('all','current_below','current_only','none') NOT NULL DEFAULT 'none',
  `nip` varchar(32) DEFAULT '-',
  `alamat` varchar(255) NOT NULL,
  `alamat_kecamatan` varchar(255) NOT NULL,
  `alamat_kabkota` varchar(255) NOT NULL,
  `alamat_provinsi` varchar(255) NOT NULL,
  `alamat_negara` varchar(255) NOT NULL,
  `alamat_kodepos` varchar(12) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenis_kelamin` int(1) NOT NULL DEFAULT 1,
  `status_pernikahan` enum('belum menikah','menikah','duda','janda') NOT NULL DEFAULT 'belum menikah',
  `telp_rumah` varchar(25) NOT NULL,
  `telp_hp` varchar(25) NOT NULL,
  `bank_rekening_nomor` varchar(255) NOT NULL,
  `bank_rekening_nama` varchar(255) NOT NULL,
  `bank_nama` varchar(255) NOT NULL,
  `kerja_terakhir` varchar(255) NOT NULL,
  `kerja_terakhir_jabatan` varchar(255) NOT NULL,
  `kerja_terakhir_gaji` float NOT NULL,
  `pendidikan_terakhir` varchar(255) NOT NULL,
  `pendidikan_terakhir_jenjang` enum('SD','SMP','SMA','S1','D3','D2','S2') NOT NULL DEFAULT 'SMA',
  `pendidikan_terakhir_tahun` year(4) NOT NULL DEFAULT 1971,
  `ibu_nama` varchar(255) NOT NULL,
  `ibu_pekerjaan` varchar(255) NOT NULL,
  `tgl_kerja_mulai` date NOT NULL,
  `tgl_kerja_akhir` date NOT NULL,
  `tgl_kontrak_akhir` date DEFAULT NULL,
  `karyawan_status` enum('Kontrak','Magang','Tetap','Harian Lepas') NOT NULL,
  `is_karyawan` int(1) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 1,
  `a_pengguna_id` int(11) DEFAULT NULL COMMENT 'atasan langsung'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabel pengguna';

--
-- Dumping data for table `a_pengguna`
--

INSERT INTO `a_pengguna` (`id`, `a_company_id`, `a_company_nama`, `a_company_kode`, `a_jabatan_id`, `a_jabatan_nama`, `username`, `password`, `email`, `nama`, `foto`, `welcome_message`, `scope`, `nip`, `alamat`, `alamat_kecamatan`, `alamat_kabkota`, `alamat_provinsi`, `alamat_negara`, `alamat_kodepos`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `status_pernikahan`, `telp_rumah`, `telp_hp`, `bank_rekening_nomor`, `bank_rekening_nama`, `bank_nama`, `kerja_terakhir`, `kerja_terakhir_jabatan`, `kerja_terakhir_gaji`, `pendidikan_terakhir`, `pendidikan_terakhir_jenjang`, `pendidikan_terakhir_tahun`, `ibu_nama`, `ibu_pekerjaan`, `tgl_kerja_mulai`, `tgl_kerja_akhir`, `tgl_kontrak_akhir`, `karyawan_status`, `is_karyawan`, `is_active`, `a_pengguna_id`) VALUES
(1, NULL, '-', '-', NULL, 'Staff', 'mimin', '21232f297a57a5a743894a0e4a801fc3', 'drosanda@outlook.co.id', 'S-Admin1', 'media/pengguna/2019/06/c4ca4238a0b923820dcc509a6f75849b3120.png', 'Selamat Beraktifitas', 'all', '-', '', '', '', '', '', '', '', NULL, 1, 'belum menikah', '', '', '', '', '', '', '', 0, '', 'SMA', 1971, '', '', '0000-00-00', '0000-00-00', NULL, 'Kontrak', 0, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `a_pengguna_module`
--

CREATE TABLE `a_pengguna_module` (
  `id` int(8) NOT NULL,
  `a_pengguna_id` int(4) NOT NULL,
  `a_modules_identifier` varchar(255) DEFAULT NULL,
  `rule` enum('allowed','disallowed','allowed_except','disallowed_except') NOT NULL,
  `tmp_active` enum('N','Y') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='hak akses pengguna';

--
-- Dumping data for table `a_pengguna_module`
--

INSERT INTO `a_pengguna_module` (`id`, `a_pengguna_id`, `a_modules_identifier`, `rule`, `tmp_active`) VALUES
(1, 1, NULL, 'allowed_except', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `a_usergroup`
--

CREATE TABLE `a_usergroup` (
  `id` int(11) NOT NULL,
  `kode` varchar(24) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `is_expired` int(1) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='jenis pelanggan, grup pelanggan';

-- --------------------------------------------------------

--
-- Table structure for table `b_menu`
--

CREATE TABLE `b_menu` (
  `id` int(11) NOT NULL,
  `posisi` enum('main','mobile','footer_1','footer_2') NOT NULL DEFAULT 'main',
  `utype` enum('main_menu','sub_menu','sub_sub_menu','sub_sub_sub_menu') NOT NULL DEFAULT 'main_menu',
  `nama` varchar(255) NOT NULL,
  `icon_class` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '#',
  `url_type` enum('internal','external') NOT NULL DEFAULT 'internal',
  `priority` int(4) NOT NULL DEFAULT 0,
  `is_has_children` int(1) NOT NULL DEFAULT 0,
  `is_opentab` int(1) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 1,
  `b_menu_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `b_menu`
--

INSERT INTO `b_menu` (`id`, `posisi`, `utype`, `nama`, `icon_class`, `url`, `url_type`, `priority`, `is_has_children`, `is_opentab`, `is_active`, `b_menu_id`) VALUES
(22, 'footer_2', 'main_menu', 'FAQ', '', 'bantuan/faq/', 'internal', 1, 0, 0, 1, NULL),
(32, 'footer_1', 'main_menu', 'Artikel', '', 'kategori/ilmu', 'internal', 1, 0, 0, 1, NULL),
(34, 'footer_1', 'main_menu', 'Daftar', '', 'bukutamu', 'internal', 3, 0, 0, 1, NULL),
(49, 'main', 'main_menu', 'Artikel', '', 'blog/kategori/ilmu', 'internal', 2, 1, 0, 1, NULL),
(50, 'main', 'sub_menu', 'Tauhid', '', 'kategori/tauhid', 'internal', 1, 0, 0, 1, 49),
(51, 'main', 'sub_menu', 'Fiqih', '', 'kategori/fiqih', 'internal', 3, 1, 0, 1, 49),
(52, 'main', 'sub_menu', 'Muamalah', '', 'kategori/tenda-ringing', 'internal', 4, 0, 0, 1, 49),
(53, 'main', 'sub_menu', 'Sejarah', '', 'kategori/sejarah', 'internal', 6, 0, 0, 1, 49),
(55, 'main', 'sub_menu', 'Khutbah Jum\'at', '', 'kategori/khutbah-jumat', 'internal', 1, 0, 0, 1, 47),
(56, 'main', 'sub_menu', 'Khutbah hari raya', '', 'kategori/khutbah-hari-raya', 'internal', 2, 0, 0, 1, 47),
(57, 'main', 'sub_menu', 'Khutbah Umum', '', 'kategori/khutbah-umum', 'internal', 3, 0, 0, 1, 47),
(58, 'main', 'main_menu', 'Daftar', '', 'bukutamu', 'internal', 1, 1, 0, 1, NULL),
(64, 'main', 'sub_menu', 'Video Tenda', '', 'https://www.youtube.com/channel/UCARBO0IS1Rs8DQjU2IYFhDw/videos', 'external', 2, 0, 0, 1, 46),
(65, 'main', 'main_menu', 'Tentang Kami', '', 'tentangkami', 'internal', 5, 0, 0, 1, NULL),
(66, 'main', 'sub_menu', 'Testimonial', '', 'testimonial/', 'internal', 3, 0, 0, 1, 46),
(67, 'main', 'sub_menu', 'Blog', '', 'blog/', 'internal', 4, 0, 0, 1, 46);

-- --------------------------------------------------------

--
-- Table structure for table `b_user`
--

CREATE TABLE `b_user` (
  `id` int(11) NOT NULL,
  `a_company_id` int(11) DEFAULT NULL,
  `a_usergroup_id` int(11) DEFAULT NULL,
  `kode` varchar(24) NOT NULL,
  `kode_lama` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fnama` varchar(255) NOT NULL,
  `lnama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `kelurahan` varchar(255) NOT NULL,
  `kecamatan` varchar(255) NOT NULL,
  `kabkota` varchar(255) NOT NULL,
  `provinsi` varchar(255) NOT NULL DEFAULT '',
  `negara` varchar(255) NOT NULL DEFAULT 'Indonesia',
  `kodepos` varchar(25) NOT NULL,
  `kelamin` int(1) NOT NULL DEFAULT 1 COMMENT '1 laki-laki 0 perempuan',
  `tlahir` varchar(255) NOT NULL DEFAULT '-' COMMENT 'tempat lahir',
  `bdate` date NOT NULL DEFAULT '1970-01-01' COMMENT 'tanggal lahir',
  `cdate` datetime NOT NULL COMMENT 'tanggal pembuatan',
  `adate` date DEFAULT NULL COMMENT 'tanggal aktifasi',
  `edate` date DEFAULT NULL COMMENT 'tanggal berakhir membership',
  `telp` varchar(25) NOT NULL,
  `fb` varchar(255) NOT NULL,
  `fb_id` int(11) DEFAULT NULL,
  `ig` varchar(255) NOT NULL,
  `ig_id` int(11) DEFAULT NULL,
  `deposit` float NOT NULL DEFAULT 0 COMMENT 'saldo_deposit',
  `poin` int(11) NOT NULL DEFAULT 0,
  `image` varchar(255) NOT NULL,
  `reg_from` varchar(64) NOT NULL DEFAULT 'online',
  `ptype` varchar(78) NOT NULL DEFAULT 'umum' COMMENT 'jenis pelanggan',
  `umur` int(3) NOT NULL DEFAULT 20,
  `pekerjaan` varchar(78) NOT NULL DEFAULT '-',
  `beli_jml` int(6) NOT NULL DEFAULT 0,
  `beli_terakhir` datetime DEFAULT NULL,
  `beli_total` float NOT NULL DEFAULT 0,
  `api_web_token` varchar(24) DEFAULT NULL,
  `api_mobile_token` varchar(24) DEFAULT NULL,
  `fcm_token` varchar(255) NOT NULL,
  `foto_before` varchar(255) NOT NULL,
  `foto_after` varchar(255) NOT NULL,
  `is_agree` int(1) NOT NULL DEFAULT 0,
  `is_confirmed` int(1) NOT NULL DEFAULT 0 COMMENT '1 ya, 0 belum konfirmasi, flag setelah konfirmasi',
  `is_premium` int(1) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabel pengguna, bisa member atau user vendor,';

-- --------------------------------------------------------

--
-- Table structure for table `b_user_alamat`
--

CREATE TABLE `b_user_alamat` (
  `id` int(11) NOT NULL,
  `b_user_id` int(11) NOT NULL,
  `nama_alamat` varchar(50) NOT NULL,
  `nama_penerima` varchar(255) NOT NULL,
  `telp` varchar(25) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kelurahan` varchar(255) NOT NULL,
  `kecamatan` varchar(255) NOT NULL,
  `kabkota` varchar(255) NOT NULL,
  `provinsi` varchar(255) NOT NULL,
  `negara` varchar(255) NOT NULL,
  `kodepos` varchar(10) NOT NULL,
  `is_default` int(1) NOT NULL DEFAULT 1,
  `is_active` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='buku alamat untuk user';

-- --------------------------------------------------------

--
-- Table structure for table `c_guestbook`
--

CREATE TABLE `c_guestbook` (
  `id` int(11) NOT NULL,
  `cdate` datetime NOT NULL,
  `nama` varchar(255) NOT NULL,
  `ttl` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `jeniskelamin` enum('Laki-laki','Perempuan','','') NOT NULL,
  `tinggi` varchar(255) NOT NULL,
  `berat` varchar(255) NOT NULL,
  `anak` varchar(255) NOT NULL,
  `saudara` varchar(255) NOT NULL,
  `telpot` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `sekolah` varchar(255) NOT NULL,
  `ijazah` varchar(255) NOT NULL,
  `skhun` varchar(255) NOT NULL,
  `nisn` varchar(255) NOT NULL,
  `alamatsekolah` varchar(255) NOT NULL,
  `telpsekolah` varchar(255) NOT NULL,
  `sarana` enum('Jalan Kaki','Transportasi Umum','Diantar Orangtua','') NOT NULL,
  `jarak` varchar(255) NOT NULL,
  `ayah` varchar(255) NOT NULL,
  `jobayah` varchar(255) NOT NULL,
  `earnayah` enum('kurang dari Rp.500.000','Rp.500.000-Rp.2.000.000','lebih dari Rp.2.000.000','') NOT NULL,
  `ibu` varchar(255) NOT NULL,
  `jobibu` varchar(255) NOT NULL,
  `earnibu` enum('kurang dari Rp.500.000','Rp.500.000-Rp.2.000.000','lebih dari Rp.2.000.000','') NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `is_subscribe` int(1) NOT NULL DEFAULT 1,
  `is_read` int(1) NOT NULL DEFAULT 0,
  `is_approved` int(1) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `c_guestbook`
--

INSERT INTO `c_guestbook` (`id`, `cdate`, `nama`, `ttl`, `agama`, `jeniskelamin`, `tinggi`, `berat`, `anak`, `saudara`, `telpot`, `alamat`, `sekolah`, `ijazah`, `skhun`, `nisn`, `alamatsekolah`, `telpsekolah`, `sarana`, `jarak`, `ayah`, `jobayah`, `earnayah`, `ibu`, `jobibu`, `earnibu`, `judul`, `isi`, `is_subscribe`, `is_read`, `is_approved`, `is_active`) VALUES
(17, '0000-00-00 00:00:00', 'sdafsdfsfgsg', 'akdfsdkfhsdiuh', 'asdfsdfsdf', 'Laki-laki', 'fisdahiusdhiusdh', 'iudfhsuihsdfaih', 'ihfdisahisdfh', 'ihifsdahidfsha', 'iuhfdisdhfifad', 'ihdfisahisdfhiuh', 'dahfuisdhfiuhsdiuhsdfiu', 'ihfdsihfiusdhishdiu', 'ihfsidhfisdhisdhfi', 'ihifudhisdhuihdsi', 'ihsdfuhdsihisdfhi', 'hiuhafihisudhfih', 'Jalan Kaki', 'hifdhsihfsdihsidfh', 'hihfdsihsdhdsf', 'ihifdhishdfihsfi', 'kurang dari Rp.500.000', 'hifhdhsdihisdfh', 'ihifsdhidsfhihdfsai', 'kurang dari Rp.500.000', '', '', 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `d_blog`
--

CREATE TABLE `d_blog` (
  `id` int(11) NOT NULL,
  `a_pengguna_id` int(4) DEFAULT NULL,
  `kategori` varchar(255) NOT NULL DEFAULT 'Uncategorized',
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `mtitle` varchar(255) NOT NULL,
  `mkeyword` varchar(255) NOT NULL,
  `mdescription` varchar(255) NOT NULL,
  `mauthor` varchar(255) NOT NULL,
  `mimage` varchar(255) NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `excerpt` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `featured_image` varchar(255) NOT NULL,
  `cdate` datetime NOT NULL,
  `ldate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `jml_baca` int(6) NOT NULL DEFAULT 0,
  `status` enum('draft','publish','private') NOT NULL DEFAULT 'draft'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_blog`
--

INSERT INTO `d_blog` (`id`, `a_pengguna_id`, `kategori`, `title`, `slug`, `mtitle`, `mkeyword`, `mdescription`, `mauthor`, `mimage`, `content`, `excerpt`, `tags`, `featured_image`, `cdate`, `ldate`, `jml_baca`, `status`) VALUES
(24, 1, 'ilmu,tauhid,manhaj,fiqih,', 'Jual Tenda Folding atau Tenda Lipat', 'jual-tenda-folding-atau-tenda-lipat', '', '', '', 'S-Admin1', 'media/upload/2018/12/tenda-folding-tenda-lipat.png', '<p><img alt=\"Jual Tenda Folding\" src=\"//www.pabriktenda.co.id/image-upload/ss.PNG\" style=\"display:block; height:492px; margin-left:auto; margin-right:auto; width:479px\" /></p>\r\n\r\n<p><strong>Jual Tenda Folding/Tenda Lipat,</strong>&nbsp;Kenapa Harus Memilih <em><strong>Tenda Folding/Tenda Lipat</strong></em> ??</p>\r\n\r\n<p style=\"text-align:center\"><strong>MUDAH &amp; PRAKTIS</strong></p>\r\n\r\n<p><em><strong>Tenda lipat/Tenda Folding</strong></em> merupakan salah satu jenis pelindung berupa atap kain terpal anti air (waterproof) yang mudah dipindahkan tergantung penggunaan seperti untuk Cafe, Dagang, Bazar. Tidak hanya itu, selain praktis dalam penggunaannya. Tenda lipat juga sangat mudah mulai dari cara Perakitan, Pemasangan, sampai Pembongkaran sangatlah mudah dan praktis bagi anda.</p>\r\n\r\n<p style=\"text-align:center\"><strong>KUAT &amp; TAHAN LAMA</strong></p>\r\n\r\n<p style=\"text-align:left\"><em><strong>Tenda lipat/Tenda Folding</strong></em> kami terbuat dari Besi Petak Hollow Hitam bukan besi putih yang dicat hitam. Dimana, ketahanan dan keawetan dari tenda lipat murah yang kami jual jauh lebih tahan lama. Sehingga, membuat tenda lipat ini menjadi Kuat dan Tahan Lama.</p>\r\n\r\n<p style=\"text-align:center\"><strong>MURAH &amp; EKONOMIS</strong></p>\r\n\r\n<p style=\"text-align:left\">Dengan berbagai ukuran dan bentuk yang sesuai dengan kebutuhan Anda. Tenda lipat/Tenda Folding memiliki harga yang Murah dan Ekonomis. Sehingga, untuk Anda yang ingin memulai usaha atau membuat bazar. Tidak usah pusing memikirkan biayanya.</p>\r\n\r\n<p style=\"text-align:left\">Tunggu apa lagi,&nbsp;Untuk pemesanan dan info lebih lanjut <em><strong>Tenda Folding/Tenda Lipat</strong></em> silahkan menguhubungi kontak kami <strong>CV.Mustika Jaya</strong> Alamat dan No tlp/whatsapp kami cantumkan di bawah.</p>\r\n\r\n<p style=\"text-align:left\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><strong>Tlp/Whatsapp : <em>+628112121958</em></strong><br />\r\n<strong>Alamat Produksi : <em>Jl. Melong No. 96 Cimahi Selatan</em></strong></p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:18pt\"><strong>CV. Mustika Jaya</strong></span></p>\r\n\r\n<p style=\"text-align:left\">&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'CV Mustika Jaya menjual beraneka macam tenda, salah satunya adalah tenda folding.', '', 'media/upload/2018/12/tenda-folding-tenda-lipat.png', '2019-05-18 11:38:57', '2019-05-18 04:38:57', 0, 'publish'),
(27, 1, 'ilmu,', 'Mengenal bermacam-macam bahan untuk Tenda', 'mengenal-bermacam-macam-bahan-untuk-tenda', '', '', '', 'S-Admin1', 'media/upload/2019/02/tenda-bandung-apa-itu-terpaulin-terpal.jpg', '<p>Tenda merupakan suatu bangunan sementara yang biasa digunakan untuk keadaan darurat dan dibangun di tempat terbuka. Jenis tenda pun bermacam-macam ada Tenda Sarnafil, Tenda Ringing, Tenda Komando, Tenda Kerucut, Tenda Piramid dan jenis tenda lainnya. Tapi apakah anda tahu bahwa tenda ini dibuat dengan bahan-bahan yang khusus? Yuk simak selengkapnya tentang bahan material yang digunakan untuk membuat tenda!</p>\r\n\r\n<h2>Terpaulin</h2>\r\n\r\n<p>Ya, pasti anda terkejut ketika membaca kata <strong>&quot;Terpaulin&quot;</strong> padahal ini sering kita kenal dengan sebutan Terpal. Terpal atau terpal adalah sebutan untuk bahan yang dibuat dari PVC alias plastik yang memiliki ketahanan terhadap air (waterproof) kuat, memiliki daya elastisitas yang tinggi serta cukup ringan untuk dibawa. Terpaulin terdiri dari beberapa jenis dan warna serta ketebalan yang berbeda-beda. Terpaulin ini juga bisa ditambahkan gambar dan dimodifikasi sesuai dengan kebutuhan.&nbsp;</p>\r\n\r\n<h3>Terpaulin Uno</h3>\r\n\r\n<p>Apa bedanya Terpaulin Uno dengan terpal biasa? Jawabanya adalah terpaulin uno itu adalah sebutan unutk terpal nomor satu dengan memiliki ketahanan, berat, dan warna yang tidak mudah luntur dan kuat. Terpaulin Uno sudah menjadi bahan dasar standar untuk tenda yang diproduksi oleh Tenda Bandung. Sehingga tidak perlu lagi untuk ragu tentang kualitas tenda kami.</p>\r\n\r\n<h2>Alumunium</h2>\r\n\r\n<p>Kalau anda berfikir bahwa kerangka tenda terbuat dari besi, itu tidak salah karena memang ada beberapa tenda yang dibuat dengan kerangka besi. Namun, apa yang terjadi jika semua kerangka tenda terbuat dari besi? Pasti sangat berat jika dipindahkan dan membuang tenaga yang cukup besar. Untuk mengatasi hal tersebut, kini kerangka tenda dibuat dengan bahan alumunium yang lebih ringan dari besi namun tetap kuat.</p>\r\n\r\n<h3>Alumunium Extrusion</h3>\r\n\r\n<p>Alumunium Extrusion adalah salah satu standar produksi yang digunakan dalam pembuatan kerangka tenda di Tenda Bandung. Extrusion atau ekstrusi dalam bahasa Indonesia, merupakan proses pencetakan alumunium dengan gaya tekanan tertentu sehingga menghasilkan bagian alumunium yang kuat. Biasanya proses ekstruksi kami terapkan untuk pembuatan rangka kaki dan rangka dinding tenda. Sehingga ketika ada goncangan atau tekanan alumunium tetap kuat dan kokoh untuk menopang tenda.</p>\r\n\r\n<h3>Alumunium Holo</h3>\r\n\r\n<p>Alumunium Holo merupakan jenis alumunium yang tidak berisi atau hollow sehingga memiliki bobot yang lebih ringan dari pada alumunium biasa. Alumiunium Holo sudah menjadi standar dalam pembuatan rangka atap untuk tenda di Tenda Bandung. Sehingga bobot rangka atap tetap ringan dan kuat.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Berikut ini adalah hal yang perlu diketahui sebelum anda memesan tenda yang sesuai dengan kebutuhan', '', 'media/upload/2019/02/tenda-bandung-apa-itu-terpaulin-terpal.jpg', '2019-05-18 11:39:17', '2019-05-18 04:39:17', 0, 'publish'),
(28, 1, 'tauhid,', 'Test Tenda Promosi 1', 'test-tenda-promosi-1', '', '', '', 'S-Admin1', 'media/upload/2019/03/tenda-bandung-co-id-tenda-promosi-jenis-dome.jpg', '<p>Test Tenda Promosi 1</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Test Tenda Promosi 1', '', 'media/upload/2019/03/tenda-bandung-co-id-tenda-promosi-jenis-dome.jpg', '2019-05-18 11:39:25', '2019-05-18 04:39:25', 0, 'publish'),
(29, 1, 'Tauhid,', 'Tenda Promosi Test 2', 'tenda-promosi-test-2', '', '', '', '', '', '<p>Tenda Promosi percobaan ke 2</p>\r\n', 'Tenda Promosi percobaan ke 2', '', 'media/upload/2019/03/somein-shafira-mobile.png', '2019-03-08 11:21:12', '2019-05-15 22:40:57', 0, 'publish'),
(30, 1, 'fiqih,', 'Tenda Promosi Test 3', 'tenda-promosi-test-3', '', '', '', 'S-Admin1', 'media/upload/2019/03/tenda-bandung-co-id-tenda-promosi-jenis-dome.jpg', '<p>Tenda Promosi Percobaan ke 3</p>\r\n', 'Tenda Promosi Percobaan 3', '', 'media/upload/2019/03/tenda-bandung-co-id-tenda-promosi-jenis-dome.jpg', '2019-05-18 11:39:58', '2019-05-18 04:39:58', 0, 'publish'),
(31, 1, 'ilmu,', 'Tenda Promosi Percobaan Ke 4', 'tenda-promosi-percobaan-ke-4', '', '', '', 'S-Admin1', 'media/upload/2019/03/tenda-bandung-co-id-tenda-promosi-jenis-dome.jpg', '<p>Tenda Promosi Percobaan Ke 4</p>\r\n', 'Tenda Promosi Percobaan Ke 4', '', 'media/upload/2019/03/tenda-bandung-co-id-tenda-promosi-jenis-dome.jpg', '2019-05-18 11:40:05', '2019-05-18 04:40:05', 0, 'publish'),
(32, 1, 'fiqih,', 'a', 'a', '', '', '', 'S-Admin1', 'media/upload/2019/05/mekkah.jpg', '<p>a</p>\r\n', '', '', 'media/upload/2019/05/mekkah.jpg', '2019-05-18 11:39:34', '2019-05-18 04:39:34', 0, 'publish'),
(33, 1, 'manhaj,', 'What is lorem lipsum in arab?', 'what-is-lorem-lipsum-in-arab', '', '', '', 'S-Admin1', 'media/upload/2019/01/glamping-cover.jpg', '<p>بل تسمّى لتقليعة بالتوقيع مدن, ما نفس يتمكن انتباه, ما أخذ مليون الضروري الولايات. عل السيء فاتّبع الا. خيار الذود تحرّك لمّ لم, كلّ المضي بأيدي ومطالبة كل, هناك العالم سنغافورة بل حين. كل ومن جيوب بالرغم. بال المحيط الخاطفة كل, أم الا ٠٨٠٤ العظمى المتحدة, أخذ صفحة التحالف للأراضي ما. به، كل ماذا غضون بتطويق, ما وأكثرها والمانيا الا, تصفح وقبل الشّعبين وفي أم.</p>\r\n\r\n<p>أضف في مسرح الجنود وقدّموا, نهاية كثيرة لهيمنة مكن تم, موالية وإقامة هو نفس. ثم هذا تحرّك للسيطرة والعتاد, قام كل ثمّة للحكومة, من لغزو والنفيس عرض. أم إيو مكّن الثانية باستحداث. أي أخر نقطة المضي, اعلان بتحدّي ومطالبة كان ثم, من وصل يونيو واتّجه. مع كلا احداث الأحمر الساحل, ويتّفق وحلفاؤها إذ بين.</p>\r\n\r\n<p>طوكيو اوروبا الشهيرة أضف ٣٠. عُقر الربيع، ومحاولة أن تحت, وتم انتهت الأرضية قد. وقد تم وتنصيب وأكثرها البولندي. دار ثم قادة الدنمارك ويكيبيديا, مما ان سقطت ليبين المضي. يقوم وعلى ويكيبيديا، دون بـ.</p>\r\n\r\n<p>تزامناً واقتصار حتى تم. مع أجزاء العناد إستيلاء لمّ. جُل وإقامة وانتهاءً والروسية في, ثمّة أدنى من كلا, ما لان مكّن ٢٠٠٤ استراليا،. ما به، إنطلاق الشتاء،.</p>\r\n\r\n<p>عن بشرية الشتاء، الإطلاق كلا, قد لأداء المشترك للمجهود بين. حين لم الله وحرمان, من الجديدة، المعاهدات ولم. ترتيب اعلان تحت أن, عن جديدة والديون اللازمة هذا, ٣٠ دول ووصف أحدث. بين ٣٠ والحزب للحكومة العسكري, قبل ما أفاق قتيل، قُدُماً, عن كانت الهجوم دول. قد كما وشعار فقامت, أن على حصدت لدحر, لم فقد وقام الطرفين الأمريكية. أم مدينة استطاعوا عدد.</p>\r\n\r\n<p>وقد الأعمال التغييرات الأوروبية، تم, أخذ ٣٠ وبعدما اليابانية, عن الشتاء، الثالث، نفس. قد تعد المسرح جديداً السيطرة, شيء بسبب ليركز العاصمة إذ, احداث فشكّل الأولى عل كما. أم ودول لغات النفط كلا. حول إعادة الشرقية ٣٠, دون قِبل العناد الأمريكي بـ.</p>\r\n\r\n<p>واُسدل الثالث التخطيط لمّ عل, جعل شدّت الجو الثالث، أن. ٣٠ تعديل المشترك مواقعها وفي, هو إحتار غريمه تلك. هذه أن ألمّ تجهيز إنطلاق, بعض بهيئة بالرغم وباستثناء لم, شعار الإمتعاض أسر أم. جعل إستعمل استرجاع إذ, بـ هامش وجزر وبولندا أخر. دون بقسوة لتقليعة ثم.</p>\r\n\r\n<p>تسمّى بالرغم لليابان في فصل, ولم نقطة وتتحمّل أوراقهم تم. وبالرغم بالمحور إذ تحت. ٣٠ ذلك مرمى الأمم, هذه وقرى الباهضة أم, كانت والتي ٣٠ إيو. أدنى بالمطالبة فصل و, من حيث سليمان، الأوروبية بالمطالبة. وتنصيب الدّفاع الا عن, ان هذا عالمية العالم بالرّغم.</p>\r\n\r\n<p>كرسي يتمكن الشطر قد هذا, فسقط العظمى والعتاد عن وفي. فصل ما يقوم أحدث اتّجة. أي أضف نقطة أراضي الفترة, ولم العالم، إستيلاء الإكتفاء في. قامت حالية على تم.</p>\r\n\r\n<p>القادة الخطّة المتّبعة في هذه, تلك و الشتاء باستحداث. ما تصفح صفحة يتم, ضرب إعلان عليها عن. أم ذلك وسفن تمهيد علاقة. ماذا ثانية والعتاد عرض ٣٠. الآلاف لإعادة بالعمل جهة بل. أهّل الهادي بين و, أم مما كانتا الأولى.</p>\r\n', '', '', 'media/upload/2019/01/glamping-cover.jpg', '2019-05-18 11:40:14', '2019-05-18 04:40:14', 0, 'publish'),
(34, 1, 'tauhid,', 'tet', 'tet', 'test', 'test', 'test', 'S-Admin1', 'media/upload/2019/01/glamping-img.jpg', '<p>أمرت الخارجية الأمريكية موظفي حكومة الولايات المتحدة من غير العاملين في حالات الطوارئ بمغادرة العراق على الفور، سواء كانوا في السفارة الأمريكية في بغداد أو في القنصلية الأمريكية في إربيل.</p>\r\n\r\n<p>وفي بيان على موقعها الإلكتروني، قالت السفارة الأمريكية في العراق إنها ستعلق بشكل مؤقت خدمات منح&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><iframe frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16063.340799370097!2d107.57359010357835!3d-6.944895780628094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e690241303f06e7%3A0x5b649a604d13b07e!2sSantosa+Hospital+Bandung+Kopo!5e0!3m2!1sid!2sid!4v1557913846923!5m2!1sid!2sid\" style=\"border:0\" width=\"600\"></iframe></p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '', 'media/upload/2019/01/glamping-img.jpg', '2019-05-18 11:39:44', '2019-05-18 04:39:44', 0, 'publish');

-- --------------------------------------------------------

--
-- Table structure for table `d_blogkategori`
--

CREATE TABLE `d_blogkategori` (
  `id` int(5) NOT NULL,
  `utype` enum('kategori','kategori_sub','kategori_sub_sub') NOT NULL DEFAULT 'kategori',
  `nama` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `mtitle` varchar(255) NOT NULL,
  `mkeyword` varchar(255) NOT NULL,
  `mdescription` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `cdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_visible` int(1) NOT NULL DEFAULT 1,
  `is_active` int(1) NOT NULL DEFAULT 1,
  `d_blogkategori_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_blogkategori`
--

INSERT INTO `d_blogkategori` (`id`, `utype`, `nama`, `slug`, `mtitle`, `mkeyword`, `mdescription`, `deskripsi`, `cdate`, `is_visible`, `is_active`, `d_blogkategori_id`) VALUES
(1, 'kategori', 'Ilmu', 'ilmu', 'Ilmu', 'Artikel tentang ilmu', 'Artikel tentang ilmu', '<p>Artikel tentang ilmu</p>\r\n', '2019-05-18 08:09:10', 1, 1, NULL),
(2, 'kategori_sub', 'Tauhid', 'tauhid', 'Tauhid', 'Tauhid', 'Tauhid', '<p>Tauhid</p>\r\n', '2019-05-18 08:09:10', 1, 1, 1),
(3, 'kategori_sub', 'Manhaj', 'manhaj', 'Manhaj', 'Manhaj', 'Manhaj', '<p>Manhaj</p>\r\n', '2019-05-18 08:09:10', 1, 1, 1),
(4, 'kategori_sub', 'Fiqih', 'fiqih', 'Fiqih', 'Fiqih', 'Fiqih', '<p>Fiqih</p>\r\n', '2019-05-18 08:09:10', 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `d_blog_blogkategori`
--

CREATE TABLE `d_blog_blogkategori` (
  `id` int(12) NOT NULL,
  `d_blogkategori_id` int(11) DEFAULT NULL,
  `d_blog_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `d_slider`
--

CREATE TABLE `d_slider` (
  `id` int(11) NOT NULL,
  `jenis` enum('web','mobile') NOT NULL DEFAULT 'web',
  `utype` enum('internal','external') NOT NULL DEFAULT 'internal',
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `priority` int(2) NOT NULL DEFAULT 0 COMMENT '0= urutan atas, 99=bawah',
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='slider promo untuk web dan mobile';

--
-- Dumping data for table `d_slider`
--

INSERT INTO `d_slider` (`id`, `jenis`, `utype`, `image`, `title`, `caption`, `url`, `priority`, `is_active`) VALUES
(9, 'web', 'external', 'media/sliders/2019/06/9.jpg', 'Penerimaan Siswa Baru', 'Penerimaan siswa SMP KP Soreang yang baru dibuka pada tanggal 17 Juni 2019, untuk itu kepada seluruh warga agar segera mendaftarkan putra dan putrinya di sekolah kami', 'bukutamu', 0, 1),
(10, 'web', 'external', 'media/sliders/2019/06/10.jpg', 'Sekolah sederhana penuh dengan warna', '', 'tentangkami', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `d_testimonial`
--

CREATE TABLE `d_testimonial` (
  `id` int(11) NOT NULL,
  `cdate` datetime NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `perusahaan` varchar(255) NOT NULL,
  `testimonial` varchar(255) NOT NULL,
  `rating` int(1) NOT NULL DEFAULT 5,
  `featured_image` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `d_testimonial`
--

INSERT INTO `d_testimonial` (`id`, `cdate`, `nama`, `jabatan`, `perusahaan`, `testimonial`, `rating`, `featured_image`, `is_active`) VALUES
(1, '0000-00-00 00:00:00', 'Reza Maulana', 'CEO', 'The Cloud Alert', 'I have been using it for a number of years. I use Colorlib for usability testing. It\'s great for taking images and making clickable image prototypes that do the job and save me the coding time and just the general', 3, '', 1),
(2, '0000-00-00 00:00:00', 'Yugie Nugraha', 'Evanglist', 'Microsoft Corp', 'Hello World', 2, '', 1),
(3, '0000-00-00 00:00:00', 'Andi Insanudin', 'CTO', 'Caterpillar Dev', 'Wololo', 4, '', 1),
(4, '0000-00-00 00:00:00', 'Bagja Gumelar', 'Founder', 'Be Hair Cut', 'Hoahoahoahoahoaha', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `e_kajian`
--

CREATE TABLE `e_kajian` (
  `id` int(11) NOT NULL,
  `a_pengguna_id` int(4) DEFAULT NULL,
  `judul` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `mauthor` varchar(76) NOT NULL,
  `mtitle` varchar(160) NOT NULL,
  `mkeyword` varchar(78) NOT NULL,
  `mdescription` varchar(255) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `cdate` datetime NOT NULL,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `narasumber` varchar(255) NOT NULL,
  `gmaps_link` varchar(255) NOT NULL,
  `gmaps_lat` decimal(14,11) NOT NULL,
  `gmaps_long` decimal(14,11) NOT NULL,
  `negara` varchar(24) NOT NULL,
  `provinsi` varchar(78) NOT NULL,
  `kabkota` varchar(128) NOT NULL,
  `kecamatan` varchar(128) NOT NULL,
  `kelurahan` varchar(78) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kodepos` varchar(8) NOT NULL,
  `telp` varchar(24) NOT NULL,
  `deskripsi` text NOT NULL,
  `featured_image` varchar(255) NOT NULL,
  `is_expired` int(1) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_kajian`
--

INSERT INTO `e_kajian` (`id`, `a_pengguna_id`, `judul`, `slug`, `mauthor`, `mtitle`, `mkeyword`, `mdescription`, `tempat`, `cdate`, `sdate`, `edate`, `narasumber`, `gmaps_link`, `gmaps_lat`, `gmaps_long`, `negara`, `provinsi`, `kabkota`, `kecamatan`, `kelurahan`, `alamat`, `kodepos`, `telp`, `deskripsi`, `featured_image`, `is_expired`, `is_active`) VALUES
(1, 1, 'Golongan yang boleh tidak shaum', 'golongan-yang-boleh-tidak-shaum', 'S-Admin1', '', '', '', 'Masjid Umar Bin Khattab', '2019-05-16 15:16:51', '2019-05-30 14:00:00', '2019-05-30 14:43:00', 'Ustadz Abu Haidar As Sundawy', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', 'Kp Selacau', '40218', '', '', 'media/upload/2019/01/glamping-cover.jpg', 0, 1),
(2, 1, 'Sholat &#38; zakat', 'sholat-38-zakat', 'S-Admin1', 'Jadwal Kajian Sholat &#38; zakat oleh Ustadz Abdullah Amir Maretan', 'Kajian Sholat', 'Jadwal Kajian Sholat &#38; zakat oleh Ustadz Abdullah Amir Maretan di Masjid Baiturrahman, Kab Bandung', 'Masjid Baiturrahman', '2019-05-18 11:17:39', '2019-05-18 12:30:00', '2019-05-18 14:30:00', 'Ustadz Abdullah Amir Maretan', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', 'Komplek Taman Kopo Indah 3', '40218', '', '<p>Sholat &amp; zakat</p>\r\n', 'media/upload/2019/05/mekkah.jpg', 0, 1),
(3, 1, 'Pemimpin Adalah Cerminan Rakyat', 'pemimpin-adalah-cerminan-rakyat', 'S-Admin1', '', '', '', 'Masjid Al Barokah', '2019-05-18 11:59:55', '2019-06-26 10:00:00', '2019-06-26 12:00:00', 'Ustadz Beni Sarbeni, LC', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(4, 1, 'Lailatul Qadar', 'lailatul-qadar', 'S-Admin1', '', '', '', 'Masjid Asy Syuhud', '2019-05-18 11:54:58', '2019-05-08 06:00:00', '2019-05-08 08:00:00', 'Ustadz Abdullah Amir Maretan', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(5, 1, 'Madrasah Ramadhan', 'madrasah-ramadhan', 'S-Admin1', '', '', '', 'Masjid Al Asyi&#39;ari', '2019-05-18 11:56:08', '2019-09-19 11:55:15', '2019-09-19 11:55:15', 'Ustadz Chandra Aditya', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(6, 1, 'Risalah Kekasih', 'risalah-kekasih', 'S-Admin1', '', '', '', 'Masjid Al Munawaroh', '2019-05-18 11:57:05', '2019-07-17 11:56:15', '2019-07-17 11:56:15', 'Ustadz Abu Nida Nana', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(7, 1, 'Madrasah Ramadhan (2)', 'madrasah-ramadhan-2', 'S-Admin1', '', '', '', 'Masjid Al Asyi&#39;ari', '2019-05-18 11:57:54', '2019-05-22 11:57:15', '2019-05-22 11:57:15', 'Ustadz Zulhendra', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(8, 1, 'Pengaruh Asma’al suhna, terhada peribadatan seorang hamba', 'pengaruh-asma-al-suhna-terhada-peribadatan-seorang-hamba', 'S-Admin1', '', '', '', 'Masjid Salma Al Farisi', '2019-05-18 11:59:21', '2019-05-28 11:58:45', '2019-05-28 11:58:45', 'Ustadz Abdillah Andi Suhandi', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(9, 1, 'Sholat &#38; zakat', 'sholat-38-zakat', 'S-Admin1', 'Jadwal Kajian Sholat &#38; zakat oleh Ustadz Abdullah Amir Maretan', 'Kajian Sholat', 'Jadwal Kajian Sholat &#38; zakat oleh Ustadz Abdullah Amir Maretan di Masjid Baiturrahman, Kab Bandung', 'Masjid Baiturrahman', '2019-05-18 11:17:39', '2019-05-18 12:30:00', '2019-05-18 14:30:00', 'Ustadz Abdullah Amir Maretan', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', 'Komplek Taman Kopo Indah 3', '40218', '', '<p>Sholat &amp; zakat</p>\r\n', 'media/upload/2019/05/mekkah.jpg', 0, 1),
(10, 1, 'Pemimpin Adalah Cerminan Rakyat', 'pemimpin-adalah-cerminan-rakyat', 'S-Admin1', '', '', '', 'Masjid Al Barokah', '2019-06-18 11:59:55', '2019-06-26 10:00:00', '2019-06-26 12:00:00', 'Ustadz Beni Sarbeni, LC', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(11, 1, 'Lailatul Qadar', 'lailatul-qadar', 'S-Admin1', '', '', '', 'Masjid Asy Syuhud', '2019-04-18 11:54:58', '2019-03-08 06:00:00', '2019-05-08 08:00:00', 'Ustadz Abdullah Amir Maretan', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(12, 1, 'Madrasah Ramadhan', 'madrasah-ramadhan', 'S-Admin1', '', '', '', 'Masjid Al Asyi&#39;ari', '2019-05-18 11:56:08', '2019-04-19 11:55:15', '2019-09-19 11:55:15', 'Ustadz Chandra Aditya', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(13, 1, 'Risalah Kekasih', 'risalah-kekasih', 'S-Admin1', '', '', '', 'Masjid Al Munawaroh', '2019-03-18 11:57:05', '2019-02-17 11:56:15', '2019-07-17 11:56:15', 'Ustadz Abu Nida Nana', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(14, 1, 'Madrasah Ramadhan (2)', 'madrasah-ramadhan-2', 'S-Admin1', '', '', '', 'Masjid Al Asyi&#39;ari', '2019-05-18 11:57:54', '2019-01-22 11:57:15', '2019-05-22 11:57:15', 'Ustadz Zulhendra', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1),
(15, 1, 'Pengaruh Asma’al suhna, terhada peribadatan seorang hamba', 'pengaruh-asma-al-suhna-terhada-peribadatan-seorang-hamba', 'S-Admin1', '', '', '', 'Masjid Salma Al Farisi', '2019-05-18 11:59:21', '2019-05-28 11:58:45', '2019-05-28 11:58:45', 'Ustadz Abdillah Andi Suhandi', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', '', '40218', '', '', 'media/uploads/default.jpg', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `e_kegiatan`
--

CREATE TABLE `e_kegiatan` (
  `id` int(11) NOT NULL,
  `a_pengguna_id` int(4) DEFAULT NULL,
  `judul` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `mauthor` varchar(76) NOT NULL,
  `mtitle` varchar(160) NOT NULL,
  `mkeyword` varchar(78) NOT NULL,
  `mdescription` varchar(255) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `cdate` datetime NOT NULL,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `gmaps_link` varchar(255) NOT NULL,
  `gmaps_lat` decimal(14,11) NOT NULL,
  `gmaps_long` decimal(14,11) NOT NULL,
  `negara` varchar(24) NOT NULL,
  `provinsi` varchar(78) NOT NULL,
  `kabkota` varchar(128) NOT NULL,
  `kecamatan` varchar(128) NOT NULL,
  `kelurahan` varchar(78) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kodepos` varchar(8) NOT NULL,
  `telp` varchar(24) NOT NULL,
  `deskripsi` text NOT NULL,
  `featured_image` varchar(255) NOT NULL,
  `is_expired` int(1) NOT NULL DEFAULT 0,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `e_kegiatan`
--

INSERT INTO `e_kegiatan` (`id`, `a_pengguna_id`, `judul`, `slug`, `mauthor`, `mtitle`, `mkeyword`, `mdescription`, `tempat`, `cdate`, `sdate`, `edate`, `gmaps_link`, `gmaps_lat`, `gmaps_long`, `negara`, `provinsi`, `kabkota`, `kecamatan`, `kelurahan`, `alamat`, `kodepos`, `telp`, `deskripsi`, `featured_image`, `is_expired`, `is_active`) VALUES
(2, 1, 'Perpisahan', 'perpisahan', 'S-Admin1', '', '', '', 'SMPKP Soreang', '2019-06-13 00:06:13', '2019-05-16 00:04:00', '2019-05-16 00:08:00', '', '-6.96644430000', '107.55165390000', 'Indonesia', 'Jawa Barat', 'Kab. Bandung', 'Margaasih', 'Desa Mekarsari', 'Jl Terusan Taman Kopo Indah III', '40218', '', '<p>Berdasarkan kalendar pendidikan,</p>\r\n', 'media/upload/2019/01/glamping-cover.jpg', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `a_company`
--
ALTER TABLE `a_company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_company_id` (`a_company_id`),
  ADD KEY `kode` (`kode`);

--
-- Indexes for table `a_config`
--
ALTER TABLE `a_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `a_grup`
--
ALTER TABLE `a_grup`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `a_jabatan`
--
ALTER TABLE `a_jabatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_company_id` (`a_company_id`);

--
-- Indexes for table `a_media`
--
ALTER TABLE `a_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `b_user_id` (`b_user_id`);

--
-- Indexes for table `a_modules`
--
ALTER TABLE `a_modules`
  ADD PRIMARY KEY (`identifier`),
  ADD KEY `children_identifier` (`children_identifier`);

--
-- Indexes for table `a_pengguna`
--
ALTER TABLE `a_pengguna`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `a_pengguna_username_unq` (`username`),
  ADD KEY `a_company_id` (`a_company_id`),
  ADD KEY `a_jabatan_id` (`a_jabatan_id`),
  ADD KEY `nip` (`nip`),
  ADD KEY `a_pengguna_id` (`a_pengguna_id`);

--
-- Indexes for table `a_pengguna_module`
--
ALTER TABLE `a_pengguna_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fka_pengguna_id` (`a_pengguna_id`),
  ADD KEY `fka_modules_identifier` (`a_modules_identifier`);

--
-- Indexes for table `a_usergroup`
--
ALTER TABLE `a_usergroup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b_menu`
--
ALTER TABLE `b_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `b_menu_id` (`b_menu_id`);

--
-- Indexes for table `b_user`
--
ALTER TABLE `b_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_api_web_token` (`api_web_token`),
  ADD KEY `kode` (`kode`),
  ADD KEY `a_company_id` (`a_company_id`),
  ADD KEY `a_usergroup_id` (`a_usergroup_id`);

--
-- Indexes for table `b_user_alamat`
--
ALTER TABLE `b_user_alamat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_b_user_id` (`b_user_id`);

--
-- Indexes for table `c_guestbook`
--
ALTER TABLE `c_guestbook`
  ADD PRIMARY KEY (`id`),
  ADD KEY `telp` (`telpot`);

--
-- Indexes for table `d_blog`
--
ALTER TABLE `d_blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`slug`),
  ADD KEY `b_user_id` (`a_pengguna_id`),
  ADD KEY `kategori` (`kategori`,`title`);

--
-- Indexes for table `d_blogkategori`
--
ALTER TABLE `d_blogkategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_blog_blogkategori`
--
ALTER TABLE `d_blog_blogkategori`
  ADD PRIMARY KEY (`id`),
  ADD KEY `d_blogkategori_id` (`d_blogkategori_id`),
  ADD KEY `d_blog_id` (`d_blog_id`);

--
-- Indexes for table `d_slider`
--
ALTER TABLE `d_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `d_testimonial`
--
ALTER TABLE `d_testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `e_kajian`
--
ALTER TABLE `e_kajian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_pengguna_id` (`a_pengguna_id`);

--
-- Indexes for table `e_kegiatan`
--
ALTER TABLE `e_kegiatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_pengguna_id` (`a_pengguna_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `a_company`
--
ALTER TABLE `a_company`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `a_config`
--
ALTER TABLE `a_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `a_grup`
--
ALTER TABLE `a_grup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `a_jabatan`
--
ALTER TABLE `a_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `a_media`
--
ALTER TABLE `a_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `a_pengguna`
--
ALTER TABLE `a_pengguna`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `a_pengguna_module`
--
ALTER TABLE `a_pengguna_module`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `a_usergroup`
--
ALTER TABLE `a_usergroup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `b_menu`
--
ALTER TABLE `b_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `b_user`
--
ALTER TABLE `b_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `b_user_alamat`
--
ALTER TABLE `b_user_alamat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `c_guestbook`
--
ALTER TABLE `c_guestbook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `d_blog`
--
ALTER TABLE `d_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `d_blogkategori`
--
ALTER TABLE `d_blogkategori`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `d_slider`
--
ALTER TABLE `d_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `d_testimonial`
--
ALTER TABLE `d_testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `e_kajian`
--
ALTER TABLE `e_kajian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `e_kegiatan`
--
ALTER TABLE `e_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
